package com.hexcode.developer.teaandglory;

/**
 * Created by MarcoBravia on 28-01-2017.
 */


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class CategoryProductAdapter extends RecyclerView.Adapter<CategoryProductAdapter.ViewHolder> {
    View view1,view2;
    ViewHolder viewHolder1;
    Context pcContext;
    String[] ProductName;
    String[] ProductImages;
    String[] ProductPrice;
    String[] ProductQnty;
    String[] ProductId;
    int pcount;
    Datavalue datavalue=new Datavalue();
    DatabaseCart databaseCart;
    String product_id,product_price,product_name,product_quantity,single_price,image;
    SharedPreferences sharedPreferenceslanguage;
    String language;
    UrlServer urlServer=new UrlServer();
    String IMGURL;

    public CategoryProductAdapter(Context context, String[] product_list, String[] product_image_list, String[] product_price_list, String[] product_qnty_list, String[] product_id_list, int count){
        pcContext=context;
        ProductName=product_list;
        ProductImages=product_image_list;
        ProductPrice=product_price_list;
        ProductQnty=product_qnty_list;
        ProductId=product_id_list;
        pcount=count;
         databaseCart=new DatabaseCart(pcContext);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_product, parent, false);
        viewHolder1 = new ViewHolder(view1);
        view1.setOnClickListener(Products.myOnClickListener);
        return viewHolder1;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        IMGURL=urlServer.getImageURL();
        Typeface face = Typeface.createFromAsset(pcContext.getAssets(),
            "LibreFranklin-Light.ttf");
        holder.textView.setText(ProductName[position]);
        holder.textView.setTypeface(face);
        holder.textView1.setText(ProductQnty[position]);
        product_quantity=holder.textView1.getText().toString();
        sharedPreferenceslanguage=pcContext.getSharedPreferences(MainPage.lang,Context.MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                holder.order.setText("Order   Qr"+" "+ProductPrice[position]);
            }
            else if("ARABIC".matches(language))
            {
                holder.order.setText("طلب   ريال قطري"+" "+ProductPrice[position]);
            }
        }
        Picasso.with(pcContext).load(IMGURL+"productimage/" + ProductImages[position]).into(holder.imageView);


        holder.btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i= Integer.parseInt(holder.textView1.getText().toString());
                i=i+1;
                holder.textView1.setText(Integer.toString(i));
                double price= Double.parseDouble(ProductPrice[position]);
                double qnty=Double.parseDouble(holder.textView1.getText().toString());
                double total=price*qnty;
                double total_round=round(total,2);
                String str_total=String.format("%.2f", total_round);
                product_price=str_total;
                if(language!=null)
                {
                    if("ENGLISH".matches(language))
                    {
                        holder.order.setText("Order   Qr"+" "+str_total);
                    }
                    else if("ARABIC".matches(language))
                    {
                        holder.order.setText("طلب   ريال قطري"+" "+str_total);
                    }
                }
            }
        });
        holder.btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.textView1.getText().equals("1"))
                {
                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {
                            Toast.makeText(pcContext, "can't do this ", Toast.LENGTH_SHORT).show();
                        }
                        else if("ARABIC".matches(language))
                        {
                            Toast.makeText(pcContext, "لا يمكنك أن تفعل هذا ", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(pcContext, "can't do this ", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    int i= Integer.parseInt(holder.textView1.getText().toString());
                    i=i-1;
                    holder.textView1.setText(Integer.toString(i));
                    double price= Double.parseDouble(ProductPrice[position]);
                    double qnty=Double.parseDouble(holder.textView1.getText().toString());
                    double total=price*qnty;
                    double total_round=round(total,2);
                    String str_total=String.format("%.2f", total_round);
                    product_price=str_total;
                    if(language!=null)
                {
                    if("ENGLISH".matches(language))
                    {
                        holder.order.setText("Order   Qr"+" "+str_total);
                    }
                    else if("ARABIC".matches(language))
                    {
                        holder.order.setText("طلب   ريال قطري"+" "+str_total);
                    }
                }
                }
            }
        });
        holder.order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    product_id=ProductId[position];
                    product_name=ProductName[position];
                single_price=ProductPrice[position];
                image=ProductImages[position];
                product_quantity=holder.textView1.getText().toString();

                double price= Double.parseDouble(single_price);
                double qnty=Double.parseDouble(product_quantity);
                double total=price*qnty;
                double total_round=round(total,2);
                String str_total=String.format("%.2f", total_round);
                product_price=str_total;
                datavalue.setProductid(product_id);
                Cursor c=databaseCart.getProductid(datavalue);
                c.moveToFirst();
                if(c!=null){
                    c.moveToFirst();
                    if(c.getCount()==0){
                            saveCartdata();
                    }else{
                            UpdateCartdata();
                    }
                }else{
                    Toast.makeText(pcContext,"no cusor value", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
    @Override
    public int getItemCount() {

        return pcount;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        public TextView textView,textView1,price,tx_desc;
        public ImageView imageView;
        public Button order,btn_plus,btn_minus;
        public ViewHolder(View v) {
            super(v);
            imageView=(ImageView)v.findViewById(R.id.product_image);
            textView = (TextView)v.findViewById(R.id.txt_prdouctnam);
            textView1 = (TextView)v.findViewById(R.id.tx_qnty);
            btn_plus=(Button)v.findViewById(R.id.btn_plus);
            btn_minus=(Button)v.findViewById(R.id.btn_minus);
            order=(Button)v.findViewById(R.id.order);
        }
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public void saveCartdata(){
        try {
            datavalue.setProductid(product_id);
            datavalue.setPrice(product_price);
            datavalue.setProduct(product_name);
            datavalue.setQuantity(product_quantity);
            datavalue.setImage(image);
            datavalue.setProductprice(single_price);
            databaseCart.insertcart(datavalue);
            databaseCart.close();
            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(pcContext, "Added To Cart", Toast.LENGTH_SHORT).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(pcContext, "أضف إلى السلة", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(pcContext, "Added To Cart", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Toast.makeText(pcContext,e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    public void UpdateCartdata(){
        try {
            datavalue.setProductid(product_id);
            datavalue.setPrice(product_price);
            datavalue.setProduct(product_name);
            datavalue.setQuantity(product_quantity);
            datavalue.setImage(image);
            datavalue.setProductprice(single_price);
            databaseCart.updatecart(datavalue);
            databaseCart.close();
            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(pcContext, "Updated To Cart", Toast.LENGTH_SHORT).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(pcContext, "محدث التسوق", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(pcContext, "Updated To Cart", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Toast.makeText(pcContext,e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
