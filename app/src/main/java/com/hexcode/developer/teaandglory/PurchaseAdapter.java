package com.hexcode.developer.teaandglory;

/**
 * Created by MarcoBravia on 28-01-2017.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.ViewHolder> {
    View view1;
    ViewHolder viewHolder1;
    Context pcContext;
    String[] PurchaseId;
    String[] CartId;
    String[] Name;
    String[] HouseNumber;
    String[] Phone;
    String[] StreetName;
    String[] Area;
    String[] StreetNumber;
    String[] Zone;
    String[] Total;
    String[] Date;
    String[] Time;
    String[] Status;
    String cart_id;
    int pcount;

    SharedPreferences sharedPreferenceslanguage;
    String language;
    public PurchaseAdapter(Context context, String[] purchase_id_list, String[] cart_id_list, String[] name_list, String[] phone_list, String[] house_num_list, String[] street_name_list,String[] area_list,String[] street_num_list,String[] zone_list, String[] total_list, String[] date_list, String[] time_list, String[] status_list, int count){
        pcContext=context;
        PurchaseId=purchase_id_list;
        CartId=cart_id_list;
        Name=name_list;
        Phone=phone_list;
        HouseNumber=house_num_list;
        StreetName=street_name_list;
        Area=area_list;
        StreetNumber=street_num_list;
        Zone=zone_list;
        Total=total_list;
        Date=date_list;
        Time=time_list;
        Status=status_list;
        pcount=count;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_history, parent, false);
        viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if("".matches(HouseNumber[position]))
        {
            holder.h_num_ll.setVisibility(View.GONE);
        }
        if("".matches(StreetName[position]))
        {
            holder.s_name_ll.setVisibility(View.GONE);
        }
        if("".matches(Area[position]))
        {
            holder.area_ll.setVisibility(View.GONE);
        }
        if("".matches(StreetNumber[position]))
        {
            holder.s_num_ll.setVisibility(View.GONE);
        }
        if("".matches(Zone[position]))
        {
            holder.zone_ll.setVisibility(View.GONE);
        }
        holder.purchaseId.setText(PurchaseId[position]);
        holder.c_name.setText(Name[position]);
        holder.c_phone.setText(Phone[position]);
        holder.c_house_num.setText(HouseNumber[position]);
        holder.c_street_name.setText(StreetName[position]);
        holder.c_area.setText(Area[position]);
        holder.c_street_num.setText(StreetNumber[position]);
        holder.c_zone.setText(Zone[position]);
        holder.c_total.setText(Total[position]);
        holder.c_date.setText(Date[position]);
        holder.c_time.setText(Time[position]);
        holder.status.setText(Status[position]);
        sharedPreferenceslanguage=pcContext.getSharedPreferences(MainPage.lang,Context.MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                holder.tx_id.setText("Purchase Id");
                holder.tx_name.setText("Name");
                holder.tx_phone.setText("Phone");
                holder.tx_house_num.setText("House Number");
                holder.tx_street_name.setText("Street Name");
                holder.tx_area.setText("Area");
                holder.tx_street_num.setText("Street Number");
                holder.tx_zone.setText("Zone");
                holder.tx_total.setText("Total");
                holder.tx_date.setText("Date");
                holder.tx_time.setText("Time");
                holder.tx_status.setText("Status");
                holder.btn_details.setText("Purchase Details");
            }
            else if("ARABIC".matches(language))
            {
                holder.tx_id.setText("شراء معرف");
                holder.tx_name.setText("الاسم");
                holder.tx_phone.setText("هاتف");
                holder.tx_house_num.setText("رقم الدار");
                holder.tx_street_name.setText("اسم الشارع");
                holder.tx_area.setText("منطقة");
                holder.tx_street_num.setText("رقم الشارع");
                holder.tx_zone.setText("منطقة");
                holder.tx_total.setText("مجموع");
                holder.tx_date.setText("تاريخ");
                holder.tx_time.setText("زمن");
                holder.tx_status.setText("الوضع");
                holder.btn_details.setText("تفاصيل شراء");
            }
        }

        holder.btn_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart_id=CartId[position];
                Intent i=new Intent(pcContext,PurchaseDetails.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("cart_id",cart_id);
                pcContext.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return pcount;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        public TextView purchaseId,c_name,c_phone,c_house_num,c_street_name,c_area,c_street_num,c_zone,c_total,c_date,c_time,status;
        public Button btn_details;
        public LinearLayout h_num_ll,s_name_ll,area_ll,s_num_ll,zone_ll;
        public TextView tx_id,tx_name,tx_phone,tx_house_num,tx_street_name,tx_area,tx_street_num,tx_zone,tx_total,tx_date,tx_time,tx_status;
        public ViewHolder(View v) {
            super(v);



            purchaseId = (TextView)v.findViewById(R.id.purchase_id);
            c_name = (TextView)v.findViewById(R.id.name);
            c_phone = (TextView)v.findViewById(R.id.phone);
            c_house_num = (TextView)v.findViewById(R.id.house_num);
            c_street_name = (TextView)v.findViewById(R.id.street_name);
            c_area = (TextView)v.findViewById(R.id.area);
            c_street_num = (TextView)v.findViewById(R.id.street_num);
            c_zone = (TextView)v.findViewById(R.id.zone);
            c_total = (TextView)v.findViewById(R.id.total);
            c_date = (TextView)v.findViewById(R.id.date);
            c_time = (TextView)v.findViewById(R.id.time);
            status=(TextView)v.findViewById(R.id.status);
            btn_details=(Button)v.findViewById(R.id.btn_details);
            tx_id = (TextView)v.findViewById(R.id.tx_id);
            tx_name = (TextView)v.findViewById(R.id.tx_name);
            tx_phone = (TextView)v.findViewById(R.id.tx_phone);
            tx_house_num = (TextView)v.findViewById(R.id.tx_house_num);
            tx_street_name = (TextView)v.findViewById(R.id.tx_street_name);
            tx_area = (TextView)v.findViewById(R.id.tx_area);
            tx_street_num = (TextView)v.findViewById(R.id.tx_street_num);
            tx_zone = (TextView)v.findViewById(R.id.tx_zone);
            tx_total = (TextView)v.findViewById(R.id.tx_total);
            tx_date = (TextView)v.findViewById(R.id.tx_date);
            tx_time = (TextView)v.findViewById(R.id.tx_time);
            tx_status = (TextView)v.findViewById(R.id.tx_status);
            h_num_ll=(LinearLayout)v.findViewById(R.id.h_num_ll);
            s_name_ll=(LinearLayout)v.findViewById(R.id.s_name_ll);
            area_ll=(LinearLayout)v.findViewById(R.id.area_ll);
            s_num_ll=(LinearLayout)v.findViewById(R.id.s_num_ll);
            zone_ll=(LinearLayout)v.findViewById(R.id.zone_ll);

            Typeface face = Typeface.createFromAsset(pcContext.getAssets(),
                    "LibreFranklin-Light.ttf");
            purchaseId.setTypeface(face);
            status.setTypeface(face);
            btn_details.setTypeface(face);
            tx_id.setTypeface(face);
            tx_name.setTypeface(face);
            tx_phone.setTypeface(face);
            tx_house_num.setTypeface(face);
            tx_street_name.setTypeface(face);
            tx_area.setTypeface(face);
            tx_street_num.setTypeface(face);
            tx_zone.setTypeface(face);
            tx_total.setTypeface(face);
            tx_date.setTypeface(face);
            tx_time.setTypeface(face);
            tx_status.setTypeface(face);
            c_name.setTypeface(face);
            c_phone.setTypeface(face);
            c_house_num.setTypeface(face);
            c_street_name.setTypeface(face);
            c_area.setTypeface(face);
            c_street_num.setTypeface(face);
            c_zone.setTypeface(face);
            c_total.setTypeface(face);
            c_date.setTypeface(face);
            c_time.setTypeface(face);
        }
    }


}
