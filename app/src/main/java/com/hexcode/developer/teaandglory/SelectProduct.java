package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SelectProduct extends AppCompatActivity {
    String product_id,sub1_id,sub2_id,sub3_id,name,price,main_image,sub1_image,sub2_image,sub3_image,description,product_quantity,
            sub1_name,sub2_name,sub3_name,product_price;
    ImageView main_product_img,sub1_img,sub2_img,sub3_img;
    TextView product_name,tx_qnty,tx_desc,msg,tx_sub1_name,tx_sub2_name,tx_sub3_name;
    Button order,minus,plus,prod_add;
    RelativeLayout sub_ll;
    int count;
    LinearLayout all_ll,sub_ll1,sub_ll2,sub_ll3;
    ProgressBar progressBar;
    SharedPreferences sharedpreferencesid;

    LinearLayout btns;

    Datavalue datavalue=new Datavalue();
    DatabaseCart databaseCart;
    SharedPreferences sharedPreferenceslanguage;
    String language;

    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    TextView similar;
    UrlServer urlServer=new UrlServer();
    String URL,IMGURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_select);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        main_product_img=(ImageView)findViewById(R.id.product_image);
        sub1_img=(ImageView)findViewById(R.id.sub1);
        sub2_img=(ImageView)findViewById(R.id.sub2);
        sub3_img=(ImageView)findViewById(R.id.sub3);
        product_name=(TextView)findViewById(R.id.tx_productname);
        order=(Button)findViewById(R.id.order);
        minus=(Button)findViewById(R.id.btn_minus);
        plus=(Button)findViewById(R.id.btn_plus);
        tx_qnty=(TextView)findViewById(R.id.tx_qnty);
        tx_desc=(TextView)findViewById(R.id.desc);
        sub_ll=(RelativeLayout)findViewById(R.id.sub_ll);
        all_ll=(LinearLayout)findViewById(R.id.all_ll);
        sub_ll1=(LinearLayout)findViewById(R.id.sub_ll1);
        sub_ll2=(LinearLayout)findViewById(R.id.sub_ll2);
        sub_ll3=(LinearLayout)findViewById(R.id.sub_ll3);
        tx_sub1_name=(TextView)findViewById(R.id.sub1_name);
        tx_sub2_name=(TextView)findViewById(R.id.sub2_name);
        tx_sub3_name=(TextView)findViewById(R.id.sub3_name);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        similar=(TextView)findViewById(R.id.similar);
        msg=(TextView)findViewById(R.id.msg);
        prod_add = (Button)findViewById(R.id.prod_add);
        btns = (LinearLayout)findViewById(R.id.btns);

        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        product_name.setTypeface(face);
        tx_qnty.setTypeface(face);
        tx_desc.setTypeface(face);
        similar.setTypeface(face);
        msg.setTypeface(face);
        tx_sub1_name.setTypeface(face);
        tx_sub2_name.setTypeface(face);
        tx_sub3_name.setTypeface(face);

        databaseCart=new DatabaseCart(SelectProduct.this);
        URL=urlServer.getUrl();
        IMGURL=urlServer.getImageURL();
        SharedPreferences prf=getSharedPreferences(TodaysSpecialFragment.id,MODE_PRIVATE);
        product_id=prf.getString("id",null);
        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
            }
        }

        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);

        isInternetOn();

        prod_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prod_add.setVisibility(View.GONE);
                plus.setVisibility(View.VISIBLE);
                minus.setVisibility(View.VISIBLE);
                tx_qnty.setVisibility(View.VISIBLE);
                btns.setVisibility(View.VISIBLE);

                product_quantity=tx_qnty.getText().toString();
                datavalue.setProductid(product_id);
                Cursor c=databaseCart.getProductid(datavalue);
                c.moveToFirst();
                if(c!=null){
                    c.moveToFirst();
                    if(c.getCount()==0){
                        saveCartdata();
                    }else{
                        UpdateCartdata();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"no cusor value", Toast.LENGTH_SHORT).show();

                }
            }
        });

        sub1_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedpreferencesid=getSharedPreferences(TodaysSpecialFragment.id,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferencesid.edit();
                editor.clear();
                editor.putString("id",sub1_id);
                editor.commit();
                finish();
                startActivity(getIntent());
            }
        });
        sub2_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedpreferencesid=getSharedPreferences(TodaysSpecialFragment.id,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferencesid.edit();
                editor.clear();
                editor.putString("id",sub2_id);
                editor.commit();
                finish();
                startActivity(getIntent());
            }
        });
        sub3_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedpreferencesid=getSharedPreferences(TodaysSpecialFragment.id,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferencesid.edit();
                editor.clear();
                editor.putString("id",sub3_id);
                editor.commit();
                finish();
                startActivity(getIntent());
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int i= Integer.parseInt(tx_qnty.getText().toString());
                i=i+1;
                tx_qnty.setText(Integer.toString(i));
                double price1= Double.parseDouble(price);
                double qnty=Double.parseDouble(tx_qnty.getText().toString());
                double total=price1*qnty;
                double total_round=round(total,2);
                String str_total=String.format("%.2f", total_round);
                if(language!=null)
                {
                    if("ENGLISH".matches(language))
                    {

                        order.setText("Order   Qr"+" "+str_total);
                    }
                    else if("ARABIC".matches(language))
                    {
                        order.setText("طلب   الريال القطري"+" "+str_total);
                    }
                }
                product_price=str_total;
                product_quantity=tx_qnty.getText().toString();
                datavalue.setProductid(product_id);
                Cursor c=databaseCart.getProductid(datavalue);
                c.moveToFirst();
                if(c!=null){
                    c.moveToFirst();
                    if(c.getCount()==0){
                        saveCartdata();
                    }else{
                        UpdateCartdata();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"no cusor value", Toast.LENGTH_SHORT).show();

                }
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tx_qnty.getText().equals("1"))
                {
                    if("ENGLISH".matches(language))
                    {

                        Toast.makeText(getApplicationContext(), "Can't do this", Toast.LENGTH_SHORT).show();
                    }
                    else if("ARABIC".matches(language))
                    {
                        Toast.makeText(getApplicationContext(), "لا يمكنك أن تفعل هذا", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Can't do this", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    int i= Integer.parseInt(tx_qnty.getText().toString());
                    i=i-1;
                    tx_qnty.setText(Integer.toString(i));
                    double price1= Double.parseDouble(price);
                    double qnty=Double.parseDouble(tx_qnty.getText().toString());
                    double total=price1*qnty;
                    double total_round=round(total,2);
                    String str_total=String.format("%.2f", total_round);
                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {

                            order.setText("Order   Qr"+" "+str_total);
                        }
                        else if("ARABIC".matches(language))
                        {
                            order.setText("طلب   الريال القطري"+" "+str_total);
                        }
                    }
                    product_price=str_total;
                    product_quantity=tx_qnty.getText().toString();
                    datavalue.setProductid(product_id);
                    Cursor c=databaseCart.getProductid(datavalue);
                    c.moveToFirst();
                    if(c!=null){
                        c.moveToFirst();
                        if(c.getCount()==0){
                            saveCartdata();
                        }else{
                            UpdateCartdata();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"no cusor value", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product_quantity=tx_qnty.getText().toString();
                datavalue.setProductid(product_id);
                Cursor c=databaseCart.getProductid(datavalue);
                c.moveToFirst();
                if(c!=null){
                    c.moveToFirst();
                    if(c.getCount()==0){
                        saveCartdata();
                    }else{
                        UpdateCartdata();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"no cusor value", Toast.LENGTH_SHORT).show();

                }
            }
        });
        main_product_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SelectProduct.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    public void saveCartdata(){
        try {
            datavalue.setProductid(product_id);
            datavalue.setPrice(product_price);
            datavalue.setProduct(name);
            datavalue.setQuantity(product_quantity);
            datavalue.setImage(main_image);
            datavalue.setProductprice(price);
            databaseCart.insertcart(datavalue);
            databaseCart.close();if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Added To Cart", Toast.LENGTH_SHORT).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "أضف إلى السلة", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    public void UpdateCartdata(){
        try {
            datavalue.setProductid(product_id);
            datavalue.setPrice(product_price);
            datavalue.setProduct(name);
            datavalue.setQuantity(product_quantity);
            datavalue.setImage(main_image);
            datavalue.setProductprice(price);
            databaseCart.updatecart(datavalue);
            databaseCart.close();
            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Updated To Cart", Toast.LENGTH_SHORT).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "محدث التسوق", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private class SelectProductsConnect extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"product.php?product_id="+product_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                if (json == null)
                {
                    all_ll.setVisibility(View.GONE);
                    sub_ll.setVisibility(View.GONE);
                    msg.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
                else
                {
                    progressBar.setVisibility(View.GONE);
                    all_ll.setVisibility(View.VISIBLE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        name = obj.getString("product_name");
                        price = obj.getString("qatar_price");
                        main_image = obj.getString("image1");
                        description = obj.getString("description");
                    }
                    count = json.length();
                    product_name.setText(name);
                    Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                            "Raleway-ExtraLight.ttf");
                    product_name.setTypeface(face);
                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {
                            order.setText("Order   Qr"+" "+price);
                        }
                        else if("ARABIC".matches(language))
                        {
                            order.setText("طلب   الريال القطري"+" "+price);
                        }
                    }
                    tx_desc.setText(description);
                    tx_qnty.setText("1");
                    product_price=price;
                    Picasso.with(getApplicationContext()).load(IMGURL+"productimage/" + main_image).into(main_product_img);

                    Log.d("card......", json.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }
    private class Check_sub_product extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"sub_product.php?product_id="+product_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                if (json == null)
                {
                    sub_ll.setVisibility(View.GONE);
                }
                else
                {
                    sub_ll.setVisibility(View.VISIBLE);
                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {
                            similar.setText("Similar Products");
                        }
                        else if("ARABIC".matches(language))
                        {
                            similar.setText("منتجات مماثلة");
                        }
                    }
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        sub1_id = obj.getString("sub1_id");
                        sub2_id = obj.getString("sub2_id");
                        sub3_id = obj.getString("sub3_id");
                    }
                    if(sub1_id.equals("")&&sub2_id.equals("")&&sub3_id.equals(""))
                    {
                        sub_ll.setVisibility(View.GONE);
                    }
                    if(sub1_id.equals(""))
                    {
                        sub_ll1.setVisibility(View.GONE);
                    }
                    else
                    {
                        new Sub1().execute();
                    }
                    if(sub2_id.equals(""))
                    {
                        sub_ll2.setVisibility(View.GONE);
                    }
                    else
                    {
                        new Sub2().execute();
                    }
                    if(sub3_id.equals(""))
                    {
                        sub_ll3.setVisibility(View.GONE);
                    }
                    else
                    {
                        new Sub3().execute();
                    }
                    Log.d("card......", json.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }
        }
    }
    public final boolean isInternetOn() {

// get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

// Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            new SelectProductsConnect().execute();
            new Check_sub_product().execute();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            return false;
        }
        return false;
    }
    private class Sub1 extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"product.php?product_id="+sub1_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                if (json == null)
                {
                    sub1_img.setVisibility(View.GONE);
                    sub_ll1.setVisibility(View.GONE);
                }
                else
                {
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        sub1_image = obj.getString("image1");
                        sub1_name = obj.getString("product_name");
                    }
                    if(sub1_image.equals(""))
                    {
                        sub1_img.setVisibility(View.GONE);
                        sub_ll1.setVisibility(View.GONE);
                    }
                    else
                    {
                        tx_sub1_name.setText(sub1_name);
                        Picasso.with(getApplicationContext()).load(IMGURL+"productimage/" + sub1_image).into(sub1_img);
                    }
                    Log.d("card......", json.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }
    private class Sub2 extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"product.php?product_id="+sub2_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                if (json == null)
                {
                    sub2_img.setVisibility(View.GONE);
                    sub_ll2.setVisibility(View.GONE);
                }
                else
                {
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        sub2_image = obj.getString("image1");
                        sub2_name = obj.getString("product_name");
                    }

                    if(sub2_image.equals(""))
                    {
                        sub2_img.setVisibility(View.GONE);
                        sub_ll2.setVisibility(View.GONE);
                    }
                    else
                    {
                        tx_sub2_name.setText(sub2_name);
                        Picasso.with(getApplicationContext()).load(IMGURL+"productimage/" + sub2_image).into(sub2_img);
                    }
                    Log.d("card......", json.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }
    private class Sub3 extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"product.php?product_id="+sub3_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                if (json == null)
                {
                    sub3_img.setVisibility(View.GONE);
                    sub_ll3.setVisibility(View.GONE);
                }
                else
                {
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        sub3_image = obj.getString("image1");
                        sub3_name = obj.getString("product_name");
                    }

                    if(sub3_image.equals(""))
                    {
                        sub3_img.setVisibility(View.GONE);
                        sub_ll3.setVisibility(View.GONE);
                    }
                    else
                    {
                        tx_sub3_name.setText(sub3_name);
                        Picasso.with(getApplicationContext()).load(IMGURL+"productimage/" + sub3_image).into(sub3_img);
                    }
                    Log.d("card......", json.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };

}

