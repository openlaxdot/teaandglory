package com.hexcode.developer.teaandglory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by hexcode on 1/2/18.
 */

public class DispatcherActivity  extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Class<?> activityClass;
        try
        {
            SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
            activityClass = Class.forName(prefs.getString("lastActivity", MainActivity.class.getName()));
        } catch (ClassNotFoundException e)
        {
            activityClass = MainActivity.class;
        }
        Intent i = new Intent(DispatcherActivity.this, activityClass);
        startActivity(i);
    }
}
