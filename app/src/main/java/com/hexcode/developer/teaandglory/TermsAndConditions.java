package com.hexcode.developer.teaandglory;


import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TermsAndConditions extends AppCompatActivity {
    SharedPreferences sharedPreferenceslanguage;
    String language;

    TextView tx;

    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(TermsAndConditions.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tx=(TextView)findViewById(R.id.tx);
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);

        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
                tx.setText("By downloading or using the app, these terms will automatically apply to you – you should make sure therefore that you read them carefully before using the app. We are offering you this app to use for your own personal use without cost, but you should be aware that you’re not allowed to copy, or modify the app, any part of the app, or our trademarks in any way. You’re not allowed to attempt to extract the source code of the app, and you also shouldn’t try to translate the app into other languages, or make derivative versions. The app itself, and all the trade marks, copyright, database rights and other intellectual property rights related to it, still belong to us.");
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
                tx.setText("بواسطة التنزيل أو استخدام التطبيق، سوف تطبق هذه الشروط تلقائيا لك - يجب عليك التأكد من أنك بالتالي قراءتها بعناية قبل استخدام التطبيق. نحن نقدم لكم هذا التطبيق لاستخدام للاستخدام الشخصي الخاص بك دون تكلفة، ولكن يجب عليك أن تدرك أنك لا يسمح لنسخ أو تعديل التطبيق، أي جزء من التطبيق، أو علامات تجارية لدينا بأي شكل من الأشكال. كنت لا يسمح لمحاولة استخلاص شفرة المصدر من التطبيق، ويجب أيضا ألا تحاول ترجمة التطبيق إلى لغات أخرى، أو جعل إصدارات مشتقة. التطبيق نفسه، وجميع العلامات التجارية وحقوق المؤلف وحقوق قواعد البيانات وغيرها من حقوق الملكية الفكرية المتصلة بها، لا تزال تنتمي إلى منزل ال شاي.");
            }
        }
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;

        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        cou.setTypeface(face);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(TermsAndConditions.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
}

