package com.hexcode.developer.teaandglory;

/**
 * Created by MarcoBravia on 28-01-2017.
 */


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    View view1;
    ViewHolder viewHolder1;
    Context pcContext;
    String[] ProductName;
    String[] ProductImages;


    Context mContext;


    String[] ProductPrice;
    String[] ProductDesc;
    int pcount;
    UrlServer urlServer=new UrlServer();
    String IMGURL;
    public CategoryAdapter(Context context, String[] product_list, String[] product_image_list, int count){
        pcContext=context;
        ProductName=product_list;
        ProductImages=product_image_list;
        pcount=count;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.category, parent, false);


        viewHolder1 = new ViewHolder(view1);
        view1.setOnClickListener(MenuCardFragment.myOnClickListener);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        IMGURL=urlServer.getImageURL();
        Typeface face = Typeface.createFromAsset(pcContext.getAssets(),
                "LibreFranklin-Light.ttf");
       /* holder.textView.setText(ProductName[position]);
        holder.textView.setTypeface(face);*/
        Picasso.with(pcContext).load(IMGURL+"category/" + ProductImages[position]).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return pcount;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        public TextView price,tx_desc;
        public ImageView imageView;
        public ViewHolder(View v) {
            super(v);
            imageView=(ImageView)v.findViewById(R.id.category_image);
           /* textView = (TextView)v.findViewById(R.id.tx_category);
            textView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                }
            });*/

         // imageView.setOnClickListener(MenuCardFragment.myOnClickListener);
        }
    }
}
