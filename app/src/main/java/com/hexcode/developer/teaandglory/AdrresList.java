package com.hexcode.developer.teaandglory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class AdrresList extends AppCompatActivity {
Cursor c;
    String[] id = new String[10000];
    String[] slno = new String[1000];
    String[] name_list = new String[1000];
    String[] phone_list = new String[1000];
    String[] house_num_list = new String[1000];
    String[] street_name_list = new String[1000];
    String[] area_list = new String[1000];
    String[] street_num_list = new String[1000];
    String[] zone_list = new String[1000];
    RecyclerView recyclerView;
    Button add_new;
    String totel;
    SharedPreferences sharedPreferences;
    String language;

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.add_button_menu,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adrres_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView=(RecyclerView)findViewById(R.id.recycleview) ;
        add_new=(Button)findViewById(R.id.add_new);
         totel=getIntent().getExtras().getString("total");

        sharedPreferences=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferences.getString("lang",null);
        if("ENGLISH".matches(language))
        {
            add_new.setText("ADD NEW ADDRESS");
            ActionBar actionBar=getSupportActionBar();
            actionBar.setTitle(R.string.app_name);
        }
        else if("ARABIC".matches(language))
        {
            add_new.setText("أضف عنوانا جديدا");
            ActionBar actionBar=getSupportActionBar();
            actionBar.setTitle(R.string.app_name_arabic);
        }

     /*   add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(AdrresList.this,OrderActivity.class);
                i.putExtra("total",totel);
                startActivity(i);
                finish();
            }
        });*/
DatabaseCart  databaseCart=new DatabaseCart(AdrresList.this);
        try {
            c = databaseCart.getAddress();
            c.moveToFirst();

            if (c.getCount() == 0) {
                Intent i=new Intent(AdrresList.this,OrderActivity.class);
                i.putExtra("total",totel);
                i.putExtra("id","");
                startActivity(i);
                finish();
            } else {

                int s = 0;

                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToPosition(i);
                    s = s + 1;
                    // totelamont=totelamont+Double.parseDouble(c.getString(3));
                    id[i] = c.getString(0);
                    slno[i] = Integer.toString(s);
                    name_list[i] = c.getString(1);
                    phone_list[i] = c.getString(2);
                    house_num_list[i] = c.getString(3);
                    street_name_list[i] = c.getString(4);
                    area_list[i] = c.getString(5);
                    street_num_list[i] = c.getString(6);
                    zone_list[i] = c.getString(7);
                }
                RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(AdrresList.this);
                AddressAdapter addressAdapter = new AddressAdapter(AdrresList.this, id, slno, name_list, phone_list, house_num_list, street_name_list, area_list, street_num_list, zone_list, c.getCount(),totel);
                recyclerView.setLayoutManager(recylerViewLayoutManager);
                recyclerView.setAdapter(addressAdapter);
                recyclerView.invalidate();
                addressAdapter.notifyDataSetChanged();
            }
            c.close();
        } catch (Exception e) {

        }

    }

    public void refresh() {

        finish();
        startActivity(getIntent());
    }
    public  void exit()
    {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_add_address)
        {
            Intent i=new Intent(AdrresList.this,OrderActivity.class);
            i.putExtra("total",totel);
            startActivity(i);
            finish();
            //return true;
        }
        return super.onOptionsItemSelected(item);
        }
}
