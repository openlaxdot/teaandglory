package com.hexcode.developer.teaandglory;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.hexcode.developer.teaandglory.R.layout.dialog;

public class MainPage extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(MainPage.this);
    JSONParser jsonParser = new JSONParser();
    int res = 0,res1=0,res2=0;
    String device_id,token;

    SharedPreferences sharedpreferenceslanguage;
    public static final String lang = "lang";
    String language;
    CustomList adapter;
    ListView list;
    String[] menu_tx_eng = {
            "Home",
            "Purchased Items",
            "About Us",
            "Share App",

            "Language Settings",
            "Account",
            "Logout"
    } ;
    String[] menu_tx_arabic = {
            "المنزل",
            "البنود التي تم شراؤها",
            "معلومات عنا",
            "إرسال الطلب",

            "اعدادات اللغة",
            "حساب",
            "تسجيل الخروج"
    } ;
    Integer[] imageId = {
            R.drawable.home_icon,
            R.drawable.purchase,
            R.drawable.about_us,
            R.drawable.share,

            R.drawable.world,
            R.drawable.round_account,
            R.drawable.a_logout
    };
    UrlServer urlServer=new UrlServer();
    String URL;

    @Override
    protected void onPause()
    {
        super.onPause();
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        list=(ListView)findViewById(R.id.list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        URL=urlServer.getUrl();

        sharedpreferenceslanguage=getSharedPreferences(lang,MODE_PRIVATE);
        language=sharedpreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
            }
        }
        else
        {
            SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
            editor.clear();
            editor.putString("lang","ENGLISH");
            editor.commit();
        }
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);
        isInternetOn();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.shitstuff);
        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff) ;

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();
        /**
         * Setup click events on the Navigation View Items.
         */

        list.setDivider(null);
        list.setDividerHeight(0);

//
//        Cursor F = databaseCart.getAddress();
//        if (F.moveToFirst()) {
//            while (!F.isAfterLast()) {
//                Log.e("data", F.getString(1));
//                Toast.makeText(this, ""+F.getString(1), Toast.LENGTH_SHORT).show();
//            }}
//

        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                adapter = new
                        CustomList(MainPage.this, menu_tx_eng, imageId);
            }
            else if("ARABIC".matches(language))
            {
                adapter = new
                        CustomList(MainPage.this, menu_tx_arabic, imageId);
            }
        }
        else {
            adapter = new
                    CustomList(MainPage.this, menu_tx_eng, imageId);
        }

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {
                    Intent i=new Intent(MainPage.this,MainPage.class);
                    startActivity(i);
                }
                if(position==1)
                {
                    Intent i=new Intent(MainPage.this,PurchaseHistory.class);
                    startActivity(i);
                }
                else if(position==2)
                {
                    Intent i=new Intent(MainPage.this,MapsActivity.class);
                    startActivity(i);
                }
                else if(position==3)
                {
                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {
                            try {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                                String sAux = "\nLet me recommend you this application\n\n";
                                sAux = sAux + "https://play.google.com/store/apps/details?id=com.md.marco.login\n\n";
                                i.putExtra(Intent.EXTRA_TEXT, sAux);
                                startActivity(Intent.createChooser(i, "Share it"));
                            } catch(Exception e) {
//e.toString();
                            }
                        }
                        else if("ARABIC".matches(language))
                        {
                            try {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name_arabic);
                                String sAux = "\nاسمحوا لي أن يوصي هذا التطبيق\n\n";
                                sAux = sAux + "https://play.google.com/store/apps/details?id=com.md.marco.login\n\n";
                                i.putExtra(Intent.EXTRA_TEXT, sAux);
                                startActivity(Intent.createChooser(i, "Share it"));
                            } catch(Exception e) {
//e.toString();
                            }
                        }
                    }
                    else
                    {
                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                            String sAux = "\nLet me recommend you this application\n\n";
                            sAux = sAux + "https://play.google.com/store/apps/details?id=com.md.marco.login\n\n";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "Share it"));
                        } catch(Exception e) {
//e.toString();
                        }
                    }
                }
               /* else if(position==3)
                {
                    Intent i=new Intent(MainPage.this,TermsAndConditions.class);
                    startActivity(i);
                }
                else if(position==4)
                {
                    Intent i=new Intent(MainPage.this,NotificationList.class);
                    startActivity(i);
                }*/
                else if(position==4)
                {
                    if(language!=null)
                    {
                       // popup();


                        if("ENGLISH".matches(language))
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
                           builder.setTitle("Language settings");
                            builder.setMessage("Select Your Language ");
                            builder.setIcon(R.drawable.icon_snackmaroc);
                            builder.setCancelable(false);
                            builder.setNegativeButton("عربى", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                                    editor.clear();
                                    editor.putString("lang","ARABIC");
                                    editor.commit();
                                    finish();
                                    startActivity(getIntent());
                                }
                            });
                            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    builder.setCancelable(true);
                                }
                            });
                            builder.setPositiveButton("ENGLISH", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                                    editor.clear();
                                    editor.putString("lang","ENGLISH");
                                    editor.commit();
                                    finish();
                                    startActivity(getIntent());
                                }
                            });
                            builder.show();
                        }
                        else if("ARABIC".matches(language))
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
                          builder.setTitle("اعدادات اللغة");
                            builder.setMessage("اختر لغتك ");
                           builder.setIcon(R.drawable.icon_snackmaroc);
                            builder.setCancelable(false);
                            builder.setNegativeButton("عربى", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                                    editor.clear();
                                    editor.putString("lang","ARABIC");
                                    editor.commit();
                                    finish();
                                    startActivity(getIntent());
                                }
                            });
                            builder.setNeutralButton("إلغاء", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    builder.setCancelable(true);
                                }
                            });
                            builder.setPositiveButton("ENGLISH", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                                    editor.clear();
                                    editor.putString("lang","ENGLISH");
                                    editor.commit();
                                    finish();
                                    startActivity(getIntent());
                                }
                            });

                            builder.show();
                        }
                    }
                    else
                    {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
                       builder.setTitle("Language settings");
                       builder.setMessage("Select Your Language ");
                       builder.setIcon(R.drawable.icon_snackmaroc);
                        builder.setCancelable(false);
                        builder.setNegativeButton("                             عربى                             ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                                editor.clear();
                                editor.putString("lang","ARABIC");
                                editor.commit();
                                finish();
                                startActivity(getIntent());
                            }
                        });
                        builder.setNeutralButton("                            Cancel                            ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                builder.setCancelable(true);
                            }
                        });
                        builder.setPositiveButton("                         ENGLISH                         ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                                editor.clear();
                                editor.putString("lang","ENGLISH");
                                editor.commit();
                                finish();
                                startActivity(getIntent());
                            }
                        });
                        builder.show();


                    }
                }
                else if (position == 5)
                {
                    SharedPreferences sharedPreferences = getSharedPreferences("Account", Context.MODE_PRIVATE);
                    String nm = sharedPreferences.getString("mail","");



                    if (nm.equals(""))
                    {
                        Intent i = new Intent(MainPage.this, SignInActivity.class);
                        startActivity(i);
                    }
                    else
                        {
                        Intent i = new Intent(MainPage.this, AccountActivity.class);
                        startActivity(i);
                    }

                }
                else if (position == 6)
                {
                    SharedPreferences sharedPreferences = getSharedPreferences("Account", Context.MODE_PRIVATE);
                    String nm = sharedPreferences.getString("name","");
                    String id1 = sharedPreferences.getString("id","");

                    if (id1.equals("1"))
                    {
                        SharedPreferences.Editor editor3 = sharedPreferences.edit();
                        editor3.clear();
                        editor3.commit();
                       // MainActivity.signOut();
                        Toast.makeText(getApplicationContext(),"Logged out",Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(MainPage.this, MainPage.class);
                        startActivity(i);
                    }
                    else
                    {
                        SharedPreferences.Editor editor3 = sharedPreferences.edit();
                        editor3.clear();
                        editor3.commit();
                      //  SignInActivity.signOut();
                        Toast.makeText(getApplicationContext(),"Logged out",Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(MainPage.this, MainPage.class);
                        startActivity(i);
                    }


                }
            }
        });

//        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//                mDrawerLayout.closeDrawers();
//
//
//
//                if (menuItem.getItemId() == R.id.nav_about) {
//                    Intent i=new Intent(MainPage.this,MapsActivity.class);
//                    startActivity(i);
//                }
//
//                if (menuItem.getItemId() == R.id.nav_purchase) {
//                    Intent i=new Intent(MainPage.this,PurchaseHistory.class);
//                    startActivity(i);
//                }
//                if (menuItem.getItemId() == R.id.nav_language) {
//                    if(language!=null)
//                    {
//                        if("ENGLISH".matches(language))
//                        {
//                            final AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
//                            builder.setTitle("Language settings");
//                            builder.setMessage("Select Your Language ");
//                            builder.setIcon(R.drawable.cup_of_tea);
//                            builder.setCancelable(false);
//                            builder.setNegativeButton("ARABIC", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
//                                    editor.clear();
//                                    editor.putString("lang","ARABIC");
//                                    editor.commit();
//                                    finish();
//                                    startActivity(getIntent());
//                                }
//                            });
//                            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    builder.setCancelable(true);
//                                }
//                            });
//                            builder.setPositiveButton("ENGLISH", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
//                                    editor.clear();
//                                    editor.putString("lang","ENGLISH");
//                                    editor.commit();
//                                    finish();
//                                    startActivity(getIntent());
//                                }
//                            });
//                            builder.show();
//                        }
//                        else if("ARABIC".matches(language))
//                        {
//                            final AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
//                            builder.setTitle("اعدادات اللغة");
//                            builder.setMessage("اختر لغتك ");
//                            builder.setIcon(R.drawable.cup_of_tea);
//                            builder.setCancelable(false);
//                            builder.setNegativeButton("عربى", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
//                                    editor.clear();
//                                    editor.putString("lang","ARABIC");
//                                    editor.commit();
//                                    finish();
//                                    startActivity(getIntent());
//                                }
//                            });
//                            builder.setNeutralButton("إلغاء", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    builder.setCancelable(true);
//                                }
//                            });
//                            builder.setPositiveButton("الإنجليزية", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
//                                    editor.clear();
//                                    editor.putString("lang","ENGLISH");
//                                    editor.commit();
//                                    finish();
//                                    startActivity(getIntent());
//                                }
//                            });
//                            builder.show();
//                        }
//                    }
//                    else
//                    {
//                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
//                        builder.setTitle("Language settings");
//                        builder.setMessage("Select Your Language ");
//                        builder.setIcon(R.drawable.cup_of_tea);
//                        builder.setCancelable(false);
//                        builder.setNegativeButton("ARABIC", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
//                                editor.clear();
//                                editor.putString("lang","ARABIC");
//                                editor.commit();
//                                finish();
//                                startActivity(getIntent());
//                            }
//                        });
//                        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                builder.setCancelable(true);
//                            }
//                        });
//                        builder.setPositiveButton("ENGLISH", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
//                                editor.clear();
//                                editor.putString("lang","ENGLISH");
//                                editor.commit();
//                                finish();
//                                startActivity(getIntent());
//                            }
//                        });
//                        builder.show();
//                    }
//                }
//                if (menuItem.getItemId() == R.id.nav_terms) {
//                    Intent i=new Intent(MainPage.this,TermsAndConditions.class);
//                    startActivity(i);
//                }
//                if (menuItem.getItemId() == R.id.nav_share) {
//                    if(language!=null)
//                    {
//                        if("ENGLISH".matches(language))
//                        {
//                            try {
//                                Intent i = new Intent(Intent.ACTION_SEND);
//                                i.setType("text/plain");
//                                i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
//                                String sAux = "\nLet me recommend you this application\n\n";
//                                sAux = sAux + "https://play.google.com/store/apps/details?id=com.md.marco.login\n\n";
//                                i.putExtra(Intent.EXTRA_TEXT, sAux);
//                                startActivity(Intent.createChooser(i, "Share it"));
//                            } catch(Exception e) {
////e.toString();
//                            }
//                        }
//                        else if("ARABIC".matches(language))
//                        {
//                            try {
//                                Intent i = new Intent(Intent.ACTION_SEND);
//                                i.setType("text/plain");
//                                i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name_arabic);
//                                String sAux = "\nاسمحوا لي أن يوصي هذا التطبيق\n\n";
//                                sAux = sAux + "https://play.google.com/store/apps/details?id=com.md.marco.login\n\n";
//                                i.putExtra(Intent.EXTRA_TEXT, sAux);
//                                startActivity(Intent.createChooser(i, "Share it"));
//                            } catch(Exception e) {
////e.toString();
//                            }
//                        }
//                    }
//                    else
//                    {
//                        try {
//                            Intent i = new Intent(Intent.ACTION_SEND);
//                            i.setType("text/plain");
//                            i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
//                            String sAux = "\nLet me recommend you this application\n\n";
//                            sAux = sAux + "https://play.google.com/store/apps/details?id=com.md.marco.login\n\n";
//                            i.putExtra(Intent.EXTRA_TEXT, sAux);
//                            startActivity(Intent.createChooser(i, "Share it"));
//                        } catch(Exception e) {
////e.toString();
//                        }
//                    }
//                }
//                if (menuItem.getItemId() == R.id.nav_notify) {
//                    Intent i=new Intent(MainPage.this,NotificationList.class);
//                    startActivity(i);
//                }
//
//                return false;
//            }
//
//        });

        /**
         * Setup Drawer Toggle of the Toolbar
         */

        android.support.v7.widget.Toolbar toolbar1 = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, toolbar1,R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainPage.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();

            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            System.exit(0);
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
        public class CheckUser extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... arg0) {


            // TODO Auto-generated method stub
            try{
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user",device_id));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"check_tokengcm.php?", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res =json.getInt("code");
            }
            catch(Exception e){
                return new String("Exception: " + e.toString());
            }
            return null;

        }
        protected void onPreExecute() {
            super.onPreExecute();
        }
        protected void onPostExecute(String result)
        {
            if(res==1)
            {
                new UpdateUser().execute();
            }
            else
            {
                new AddUser().execute();
            }
        }

    }
    public class AddUser extends AsyncTask<String, Void, String>
    {
        //  ProgressDialog dialog = new ProgressDialog(MainPage.this);


        @Override
        protected String doInBackground(String... arg0) {


            // TODO Auto-generated method stub
            try{
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user",device_id));
                param.add(new BasicNameValuePair("token",token));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"add_tokengcm.php?", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res1 =json.getInt("code");
            }
            catch(Exception e){
                return new String("Exception: " + e.toString());
            }
            return null;

        }
        protected void onPreExecute() {


//            dialog.setCancelable(false);
//            dialog.setMessage("Please wait...");
//            dialog.show();

            //show dialog

            super.onPreExecute();
        }
        protected void onPostExecute(String result)
        {
            //  dialog.dismiss();
            if(res1==1)
            {

            }
            else
            {

            }
        }

    }
    public class UpdateUser extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... arg0) {

            // TODO Auto-generated method stub
            try{
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("user",device_id));
                param.add(new BasicNameValuePair("token",token));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"update_tokengcm.php?", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res2 =json.getInt("code");
            }
            catch(Exception e){
                return new String("Exception: " + e.toString());
            }
            return null;

        }
        protected void onPreExecute() {

            super.onPreExecute();
        }
        protected void onPostExecute(String result)
        {
            if(res==1)
            {

            }
            else
            {

            }
        }

    }
    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            Handler h=new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    token= FirebaseInstanceId.getInstance().getToken();
                    new CheckUser().execute();
                }
            }, 5000);


            return true;
        } else if (

                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }
    private void popup()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(dialog, null);
        dialogBuilder.setView(dialogView);

        final Button eng = (Button) dialogView.findViewById(R.id.eng);
        final Button arab = (Button) dialogView.findViewById(R.id.arab);


       /* dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");*/
        /*dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });*/

        eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                editor.clear();
                editor.putString("lang","ENGLISH");
                editor.commit();
                finish();
                startActivity(getIntent());
            }
        });
        arab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedpreferenceslanguage.edit();
                editor.clear();
                editor.putString("lang","ARABIC");
                editor.commit();
                finish();
                startActivity(getIntent());
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

}
