package com.hexcode.developer.teaandglory;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MenuCardFragment extends Fragment {
    int count=0,count3=0;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView msg;
    String[] product_list = new String[5000];
    String[] product_image_list = new String[5000];
    String[] product_id_list = new String[5000];
    Context context;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    CategoryAdapter categoryAdapter;
    static View.OnClickListener myOnClickListener;

    ImageView bag;
    int searchcount=0;
    ArrayAdapter<String> adapter1;

    String product_id;
    TextView cou;
    SharedPreferences sharedPreferenceslanguage;
    String language;
    UrlServer urlServer=new UrlServer();
    String URL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_menu_card_fragment, container, false);
        progressBar=(ProgressBar)view.findViewById(R.id.progressBar1);
        msg=(TextView)view.findViewById(R.id.msg);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        context = getActivity();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        myOnClickListener = new MyOnClickListener(getActivity());
        URL=urlServer.getUrl();
        isInternetOn();


        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),
                "LibreFranklin-Light.ttf");
        msg.setTypeface(face);

        return  view;
    }
    private class MyOnClickListener implements View.OnClickListener {
        private final Context context;
        private MyOnClickListener(Context context) {
            this.context = context;
        }
        @Override
        public void onClick(View v) {
            int selectedItemPosition = recyclerView.getChildPosition(v);
            int postion = selectedItemPosition;
            Intent i = new Intent(getActivity(), Products.class);
            Toast.makeText(getActivity(),product_id_list[postion] + product_list[postion],Toast.LENGTH_SHORT).show();
            i.putExtra("category_id", product_id_list[postion]);
            i.putExtra("category_name", product_list[postion]);
            startActivity(i);

        }
    }

    private class ProductConnect extends AsyncTask<String, String, JSONArray> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"category.php");


            return json;

        }

        @Override
        protected void onPostExecute(JSONArray json) {

            progressBar.setVisibility(View.GONE);
            try {
                if(json==null){
                    msg.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else {
//                    new Products.SubCategory().execute();
                    recyclerView.setVisibility(View.VISIBLE);
                    msg.setVisibility(View.GONE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String product_name = obj.getString("category_name");
                        String product_image = obj.getString("image");
                        String id = obj.getString("category_id");
                        product_list[i] = product_name;
                        product_image_list[i] = product_image;
                        product_id_list[i] = id;
                    }
                    count = json.length();

                    recylerViewLayoutManager = new GridLayoutManager(context, 1);
                    recyclerView.setLayoutManager(recylerViewLayoutManager);

                    categoryAdapter = new CategoryAdapter(context, product_list, product_image_list, count);
                    recyclerView.setAdapter(categoryAdapter);
                    Log.d("card......", json.toString());

                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            new ProductConnect().execute();


            return true;

        } else if (

                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getActivity(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getActivity(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getActivity(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }







}
