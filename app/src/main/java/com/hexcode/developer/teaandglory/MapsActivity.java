package com.hexcode.developer.teaandglory;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    TextView shop_name,shop_address,shop_phone,shop_phone1,msg;
    //    ListView listView;
    ProgressBar progressBar;
    LinearLayout all_ll;
    String[] name_list=new String[1000];
    String[] address_list=new String[1000];
    String[] phone_list=new String[1000];
    String[] lat_list=new String[100];
    String[] long_list=new String[100];
    String[] image_list=new String[100];
    String lat1,lon1;
    Double lat,lon;
    int count,count1;
    String name,address,phonenumber,phonenumber1,image,latitude,longitude;
    android.widget.ImageView img;
    ScrollView scroll_map;
    String languages;
    String URL,IMGURL;
    UrlServer urlServer=new UrlServer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        shop_name=(TextView)findViewById(R.id.shop_name);
        shop_address=(TextView)findViewById(R.id.shop_address);
        shop_phone=(TextView)findViewById(R.id.shop_phone);
        shop_phone1=(TextView)findViewById(R.id.shop_phone1);
        msg=(TextView)findViewById(R.id.msg);
//        listView=(ListView)findViewById(R.id.listView);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        all_ll=(LinearLayout)findViewById(R.id.lle);
        img=(ImageView) findViewById(R.id.shop_img);
        scroll_map=(ScrollView)findViewById(R.id.scroll_map);
        List<String> list = new ArrayList<String>();


        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        shop_name.setTypeface(face);
        shop_address.setTypeface(face);
        shop_phone.setTypeface(face);
        shop_phone1.setTypeface(face);
        msg.setTypeface(face);


        URL=urlServer.getUrl();
        IMGURL=urlServer.getImageURL();

        SharedPreferences prf = getSharedPreferences(MainPage.lang, MODE_PRIVATE);
        //   SharedPreferences.Editor editor = prf.edit();
        languages = prf.getString("lang", null);
        if ("ARABIC".matches(languages)) {

            TextView shname=(TextView)findViewById(R.id.tx_shop_name);
            TextView shno=(TextView)findViewById(R.id.tx_shop_phone);
            TextView shno1=(TextView)findViewById(R.id.tx_shop_phone1);
            TextView shaddress=(TextView)findViewById(R.id.tx_shop_address);
            shname.setText("اسم المحل");
            shno.setText("متجر رقم الهاتف");
            shno1.setText("متجر رقم الهاتف");
            shaddress.setText("متجر العناوين");
        } else if ("ENGLISH".matches(languages)) {
            TextView shname=(TextView)findViewById(R.id.tx_shop_name);
            TextView shno=(TextView)findViewById(R.id.tx_shop_phone);
            TextView shno1=(TextView)findViewById(R.id.tx_shop_phone1);
            TextView shaddress=(TextView)findViewById(R.id.tx_shop_address);
            shname.setText("Name");
            shno.setText("Phone No");
            shno1.setText("Phone No");
            shaddress.setText("Address");
        }



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        isInternetOn();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if(lat==null||lon==null)
        {
            View frag = findViewById(R.id.map);
            frag.setVisibility(View.GONE);
        }
        else
        {
            LatLng location = new LatLng(lat, lon);
            mMap.addMarker(new MarkerOptions().position(location).title(name_list[0]));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location,12));

        }
    }
    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            new MapConnect().execute();
            new Getlatlon().execute();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {
            if(languages!=null)
            {
                if("ENGLISH".matches(languages))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(languages))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }



    private class MapConnect extends AsyncTask<String, String, JSONArray> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json5 = jParser.getJSONFromUrl(URL+"shop_details.php?");


            return json5;

        }

        @Override
        protected void onPostExecute(JSONArray json5) {
            progressBar.setVisibility(View.GONE);
            try {
                if(json5==null){
                    all_ll.setVisibility(View.GONE);
                    msg.setVisibility(View.VISIBLE);
                }else {
                    scroll_map.setVisibility(View.VISIBLE);
                    all_ll.setVisibility(View.VISIBLE);
                    for (int i = 0; i < json5.length(); i++) {
                        JSONObject obj = json5.getJSONObject(i);
                        name = obj.getString("name");
                        address = obj.getString("address");
                        phonenumber = obj.getString("phone_number");
                        phonenumber1 = obj.getString("phonenumber1");
                        image = obj.getString("shop_image");
//                         latitude = obj.getString("latitude");
//                         longitude = obj.getString("longitude");
                        name_list[i]=name;
                        lat_list[i]=latitude;
                        long_list[i]=longitude;

                    }
                    shop_name.setText(name);
                    shop_address.setText(address);
                    shop_phone.setText(phonenumber);
                    shop_phone1.setText(phonenumber1);
                    Picasso.with(getApplicationContext()).load(IMGURL+"shop/" + image).into(img);

                    count = json5.length();
                    Log.d("card... ...", json5.toString());
                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }
    private class Getlatlon extends AsyncTask<String, String, JSONArray> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"map_details.php?");


            return json;

        }

        @Override
        protected void onPostExecute(JSONArray json) {
            progressBar.setVisibility(View.GONE);
            try {
                if(json==null){
                    View frag = findViewById(R.id.map);
                    frag.setVisibility(View.GONE);
                }else {
                    View frag = findViewById(R.id.map);
                    frag.setVisibility(View.VISIBLE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        latitude = obj.getString("latitude");
                        longitude = obj.getString("longitude");

                        lat_list[i]=latitude;
                        long_list[i]=longitude;

                    }
                    lat1=lat_list[0];
                    lon1=long_list[0];
                    if(lat1.equals("")||lat1.equals("0")||lon1.equals("")||lon1.equals("0"))
                    {
                        //View frag = findViewById(R.id.map);
                        //  frag.setVisibility(View.GONE);
                        if(languages!=null)
                        {
                            if("ENGLISH".matches(languages))
                            {
                                Toast.makeText(getApplicationContext(), "No latitude and longitude given", Toast.LENGTH_LONG).show();
                            }
                            else if("ARABIC".matches(languages))
                            {
                                Toast.makeText(getApplicationContext(), "لا خطوط الطول والعرض نظرا", Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "No latitude and longitude given", Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        lat=Double.parseDouble(lat1);
                        lon=Double.parseDouble(lon1);
                    }

                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    mapFragment.getMapAsync(MapsActivity.this);

                    count1 = json.length();
                    Log.d("card... ...", json.toString());
                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
