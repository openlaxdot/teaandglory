package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TodaysSpecialFragment extends Fragment {
    int count=0,count3=0;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView msg;
    String[] product_list = new String[5000];
    String[] product_image_list = new String[5000];
    String[] product_id_list = new String[5000];
    String[] product_qnty_list = new String[5000];
    String[] product_desc_list = new String[5000];
    String[] product_price_list = new String[5000];
    Context context;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    SpecialProductAdapterNew specialproductAdapternew;
    static View.OnClickListener myOnClickListener;

    ImageView bag;
    int searchcount=0;
    ArrayAdapter<String> adapter1;

    String product_id;
    TextView cou,tx_specials;


    SharedPreferences sharedpreferencesid,sharedPreferenceslanguage;
    public static final String id = "id";
    String language;
    UrlServer urlServer=new UrlServer();
    String URL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_todays_special_fragment, container, false);
        progressBar=(ProgressBar)view.findViewById(R.id.progressBar1);
        tx_specials=(TextView)view.findViewById(R.id.tx_specials);
        msg=(TextView)view.findViewById(R.id.msg);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        context = getActivity();
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),
                "LibreFranklin-Light.ttf");
        tx_specials.setTypeface(face);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        myOnClickListener = new MyOnClickListener(getActivity());
        URL=urlServer.getUrl();
        sharedPreferenceslanguage=getActivity().getSharedPreferences(MainPage.lang, Context.MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                tx_specials.setText("SPECIALS");
            }
            else if("ARABIC".matches(language))
            {
                tx_specials.setText(R.string.specials);
            }
        }


        isInternetOn();
        return  view;
    }
    private class MyOnClickListener implements View.OnClickListener {
        private final Context context;
        private MyOnClickListener(Context context) {
            this.context = context;
        }
        @Override
        public void onClick(View v) {
            sharedpreferencesid =getActivity().getSharedPreferences(id,Context.MODE_PRIVATE);
            product_id=sharedpreferencesid.getString("id",null);
            if(product_id!=null)
            {
                int selectedItemPosition = recyclerView.getChildPosition(v);
                int postion = selectedItemPosition;
                product_id=product_id_list[postion];
                sharedpreferencesid=getActivity().getSharedPreferences(id,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferencesid.edit();
                editor.clear();
                editor.putString("id",product_id);
                editor.commit();
            }
            else
            {
                int selectedItemPosition = recyclerView.getChildPosition(v);
                int postion = selectedItemPosition;
                product_id=product_id_list[postion];
                sharedpreferencesid=getActivity().getSharedPreferences(id,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferencesid.edit();
                editor.clear();
                editor.putString("id",product_id);
                editor.commit();
            }

            Intent i = new Intent(getActivity(), SelectProduct.class);
            startActivity(i);


        }
    }
    private class ProductConnect extends AsyncTask<String, String, JSONArray> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"special_products.php");
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {

            progressBar.setVisibility(View.GONE);
            try {
                if(json==null){
                    msg.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else {
//                    new Products.SubCategory().execute();
                    recyclerView.setVisibility(View.VISIBLE);
                    msg.setVisibility(View.GONE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String product_name = obj.getString("product_name");
                        String product_image = obj.getString("image1");
                        String price = obj.getString("qatar_price");
                        String id= obj.getString("product_id");
                        product_list[i] = product_name;
                        product_image_list[i] = product_image;
                        product_price_list[i] = price;
                        product_qnty_list[i]="1";
                        product_id_list[i]=id;
                    }
                    count = json.length();

                    recylerViewLayoutManager = new GridLayoutManager(getActivity(), 1);
                    recyclerView.setLayoutManager(recylerViewLayoutManager);

                    specialproductAdapternew = new SpecialProductAdapterNew(getActivity(), product_list, product_image_list,product_price_list,product_qnty_list,product_id_list,count);
                    recyclerView.setAdapter(specialproductAdapternew);
                    Log.d("card......", json.toString());

                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            new ProductConnect().execute();


            return true;

        } else if (

                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getActivity(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getActivity(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            return false;
        }
        return false;
    }







}
