package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Products extends AppCompatActivity {
    ProgressBar progressBar;
    TextView msg,category_name;
    LinearLayout p_ll,main_ll;
    ListView p_list;
    RecyclerView recyclerView;
    int count;
    String category_id,category;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    CategoryProductAdapter categoryProductAdapter;
    static View.OnClickListener myOnClickListener;
    String[] product_list = new String[5000];
    String[] product_image_list = new String[5000];
    String[] product_qnty_list = new String[5000];
    String[] product_id_list = new String[5000];
    String[] product_price_list = new String[5000];


    String product_id,language;
    SharedPreferences sharedpreferencesid,sharedPreferenceslanguage;

    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(Products.this);
    UrlServer urlServer=new UrlServer();
    String URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=(ProgressBar)findViewById(R.id.progressBar1);
        msg=(TextView)findViewById(R.id.msg);
        p_ll=(LinearLayout)findViewById(R.id.BACK3);
        main_ll=(LinearLayout)findViewById(R.id.main_ll);
        p_list=(ListView)findViewById(R.id.list);
        category_name=(TextView)findViewById(R.id.tx_category);
        recyclerView=(RecyclerView)findViewById(R.id.recycleview);
        category_id=getIntent().getExtras().getString("category_id");
        category=getIntent().getExtras().getString("category_name");
        myOnClickListener = new MyOnClickListener(Products.this);
        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        category_name.setTypeface(face);
        msg.setTypeface(face);



        URL=urlServer.getUrl();
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);
        isInternetOn();
        category_name.setText(category);
        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {

                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
            }
            else if("ARABIC".matches(language))
            {

                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
            }
        }
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Products.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            new ProductConnect().execute();


            return true;

        } else if (

                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            return false;
        }
        return false;
    }

    private class ProductConnect extends AsyncTask<String, String, JSONArray> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"category_products.php?category_id="+category_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {

            progressBar.setVisibility(View.GONE);
            try {
                if(json==null){
                    msg.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else {
//                    new Products.SubCategory().execute();
                    recyclerView.setVisibility(View.VISIBLE);
                    msg.setVisibility(View.GONE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String product_name = obj.getString("product_name");
                        String product_image = obj.getString("image1");
                        String price = obj.getString("qatar_price");
                        String id= obj.getString("product_id");
                        product_list[i] = product_name;
                        product_image_list[i] = product_image;
                        product_price_list[i] = price;
                        product_qnty_list[i]="1";
                        product_id_list[i]=id;
                    }
                    count = json.length();

                    recylerViewLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(recylerViewLayoutManager);

                    categoryProductAdapter = new CategoryProductAdapter(getApplicationContext(), product_list, product_image_list,product_price_list,product_qnty_list,product_id_list,count);
                    recyclerView.setAdapter(categoryProductAdapter);
                    Log.d("card......", json.toString());

                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        private final Context context;
        private MyOnClickListener(Context context) {
            this.context = context;
        }
        @Override
        public void onClick(View v) {

            int selectedItemPosition = recyclerView.getChildPosition(v);
            int postion = selectedItemPosition;
            product_id=product_id_list[postion];
            sharedpreferencesid=getSharedPreferences(TodaysSpecialFragment.id,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferencesid.edit();
            editor.clear();
            editor.putString("id",product_id);
            editor.commit();
            Intent i=new Intent(Products.this,SelectProduct.class);
            startActivity(i);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
}
