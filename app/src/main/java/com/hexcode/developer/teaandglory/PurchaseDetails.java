package com.hexcode.developer.teaandglory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PurchaseDetails extends AppCompatActivity {
    String cart_id;

    String[] product_list = new String[5000];
    String[] quantity_list = new String[5000];
    String[] price_list = new String[5000];
    int count;
    ProgressBar progressBar;
    TextView msg;
    LinearLayout p_ll,main_ll;
    ListView p_list;
    String device_id;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    PurchaseDetailsAdapter purchascedetailsAdapter;
    SharedPreferences sharedPreferenceslanguage;
    String language;
    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(PurchaseDetails.this);
    UrlServer urlServer=new UrlServer();
    String URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_details);
        cart_id=getIntent().getExtras().getString("cart_id");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=(ProgressBar)findViewById(R.id.progressBar1);
        msg=(TextView)findViewById(R.id.msg);
        p_ll=(LinearLayout)findViewById(R.id.BACK3);
        main_ll=(LinearLayout)findViewById(R.id.main_ll);
        p_list=(ListView)findViewById(R.id.list);
        recyclerView=(RecyclerView)findViewById(R.id.recycleview);
        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        URL=urlServer.getUrl();
        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
            }
        }
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);

        isInternetOn();
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(PurchaseDetails.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            new ProductConnect().execute();
            return true;
        } else if (

                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            return false;
        }
        return false;
    }
    private class ProductConnect extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"cart_details.php?cart_id="+cart_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {


            try {
                if(json==null){
                    Toast.makeText(getApplicationContext(), "no cart details", Toast.LENGTH_SHORT).show();
                }else {
//                    new Products.SubCategory().execute();
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String product = obj.getString("product");
                        String quantity = obj.getString("quantity");
                        String price = obj.getString("price");
                        product_list[i]=product;
                        quantity_list[i]=quantity;
                        price_list[i]=price;
                    }
                    count = json.length();
                    recylerViewLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(recylerViewLayoutManager);
                    purchascedetailsAdapter = new PurchaseDetailsAdapter(getApplicationContext(),product_list,quantity_list,price_list,count);
                    recyclerView.setAdapter(purchascedetailsAdapter);

                    Log.d("card......", json.toString());

                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
}
