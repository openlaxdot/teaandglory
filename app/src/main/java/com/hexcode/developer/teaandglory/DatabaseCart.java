package com.hexcode.developer.teaandglory;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseCart  extends SQLiteOpenHelper {
private static String Databsename="Cartdatabse";
    private static int Databaseverion=1;
    public static final String pursed_table="tbl_purchaset";
    public static final String tablename="tbl_cart";
    public static final String id="id";
    public static final String product="product";
    public static final String quantity="quantyty";
    public static final String price="price";
    public static final String productid="productid";
    public static final String comment="comment";
    public static final String status="status";
    public static final String productprice="productprice";
    public static final String imageproduct="image";
    public static final String noti_table="tbl_notif";
    public static final String address_tbl="tbladdress";
    public static final String house_num="housenum";
    public static final String street_name="streetname";
    public static final String area="area";
    public static final String street_num="streetnum";
    public static final String zone="zone";

    public static final String name="username";
    public static final String phone="userphone";

    String Create_Table_cart="CREATE TABLE "+tablename+"("+id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+product+" TEXT,"+quantity+" TEXT,"+price+" TEXT,"+productid+" TEXT,"+comment+" TEXT,"+productprice+" TEXT,"+imageproduct+" TEXT,"+status+" TEXT)";

    String Create_Table_purchased="CREATE TABLE "+pursed_table+"("+id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+product+" TEXT,"+quantity+" TEXT,"+price+" TEXT,"+productid+" TEXT,"+comment+" TEXT)";

    String Create_TAble_notifi="CREATE TABLE "+noti_table+"("+id+" TEXT)";


    String Create_Table_Address="CREATE TABLE "+address_tbl+"("+id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+name+" TEXT,"+phone+" TEXT,"+house_num+" TEXT,"+street_name+" TEXT,"+area+" TEXT,"+street_num+" TEXT,"+zone+" TEXT)";
    //String Create_Table_Address="CREATE TABLE "+address_tbl+"("+id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+name+" TEXT,"+phone+" TEXT,"+house_num+" TEXT,"+street_name+" TEXT,"+area+" TEXT,"+street_num+" TEXT,"+zone+" TEXT)";

    public DatabaseCart(Context context) {
        super(context, Databsename,null ,Databaseverion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_Table_cart);
        db.execSQL(Create_Table_purchased);
        db.execSQL(Create_TAble_notifi);
        db.execSQL(Create_Table_Address);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void insertcart(Datavalue datavalue){
        ContentValues cv=new ContentValues();
        SQLiteDatabase db=getWritableDatabase();
        cv.put(productid,datavalue.getProductid());
        cv.put(product,datavalue.getProduct());
        cv.put(quantity,datavalue.getQuantity());
        cv.put(price,datavalue.getPrice());
        cv.put(productprice,datavalue.getProductprice());
        cv.put(imageproduct,datavalue.getImage());
        cv.put(status,datavalue.getStatus());
        db.insert(tablename,null,cv);
        db.close();
    }
    public Cursor getcount(){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("SELECT Count(*) FROM "+tablename,null);
    }
public  void insertNotificatio(Datavalue datavalue){
    ContentValues cv=new ContentValues();
    SQLiteDatabase db=getWritableDatabase();
    cv.put(id,datavalue.getId());
    db.insert(noti_table,null,cv);
    db.close();

}
    public void insertpurcahse(Datavalue datavalue){
        ContentValues cv=new ContentValues();
        SQLiteDatabase db=getWritableDatabase();
        cv.put(productid,datavalue.getProductid());
        cv.put(product,datavalue.getProduct());
        cv.put(quantity,datavalue.getQuantity());
        cv.put(price,datavalue.getPrice());
        db.insert(pursed_table,null,cv);
        db.close();
    }
    public Cursor getProductid(Datavalue datavalue){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select * from tbl_cart where productid = '"+datavalue.getProductid()+"' ",null);
    }
    public Cursor getNotificatioid(Datavalue datavalue){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select * from tbl_notif where id = '"+datavalue.getProductid()+"' ",null);
    }

    public Cursor getTotel(){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select sum(price) from tbl_cart",null);
    }
    public Cursor getProducts(){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select * from tbl_cart ",null);
    }
    public Cursor getpurchaseProducts(){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select * from tbl_purchaset ",null);
    }

    public void updatecart(Datavalue datavalue){
        ContentValues cv=new ContentValues();
        SQLiteDatabase db=getWritableDatabase();
        cv.put(productid,datavalue.getProductid());
        cv.put(product,datavalue.getProduct());
        cv.put(quantity,datavalue.getQuantity());
        cv.put(price,datavalue.getPrice());
        cv.put(status,datavalue.getStatus());
        db.update(tablename,cv,"productid = '"+datavalue.getProductid()+"'",null);
                db.close();
    }
    public void deleteCart(Datavalue datavalue){
        SQLiteDatabase db=getWritableDatabase();
        db.delete(tablename,"productid = '"+datavalue.getProductid()+"'",null);
        db.close();
    }
    public void deleteaddress(Datavalue datavalue){
        SQLiteDatabase db=getWritableDatabase();
        db.delete(address_tbl,"id = '"+datavalue.getId()+"'",null);
        db.close();
    }
    public void deleteall(){
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("delete from "+tablename);
        db.close();
    }
    public void deleteallpurchase(){
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("delete from "+pursed_table);
        db.close();
    }

    public void InsertAddress(Datavalue datavalue)
    {
        ContentValues cv=new ContentValues();
        SQLiteDatabase db=getWritableDatabase();

        cv.put(name,datavalue.getUsername());
        cv.put(phone,datavalue.getPhone());
        cv.put(house_num,datavalue.getHouse_num());
        cv.put(street_name,datavalue.getStreet_name());
        cv.put(area,datavalue.getArea());
        cv.put(street_num,datavalue.getStreet_num());
        cv.put(zone,datavalue.getZone());
        db.insert(address_tbl,null,cv);
        db.close();
    }
public void updateaddress(Datavalue datavalue){
    ContentValues cv=new ContentValues();
    SQLiteDatabase db=getWritableDatabase();
    cv.put(name,datavalue.getUsername());
    cv.put(phone,datavalue.getPhone());
    cv.put(house_num,datavalue.getHouse_num());
    cv.put(street_name,datavalue.getStreet_name());
    cv.put(area,datavalue.getArea());
    cv.put(street_num,datavalue.getStreet_num());
    cv.put(zone,datavalue.getZone());
    db.update(address_tbl,cv,"id = '"+datavalue.getId()+"'",null);
    db.close();
}

    public Cursor getid(Datavalue datavalue){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select * from tbladdress where id = '"+datavalue.getId()+"' ",null);
    }

    public Cursor getAddress(){
        SQLiteDatabase db=getReadableDatabase();
        return db.rawQuery("select * from tbladdress",null);
    }
}
