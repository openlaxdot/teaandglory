package com.hexcode.developer.teaandglory;


import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationDetails extends AppCompatActivity {
    ProgressBar progressBar;
    ScrollView scrollView;
    String notify_id,title,content,datetime,image;
    android.widget.ImageView img;
    TextView tx_title,tx_msg,tx_datetime;
    // android.widget.ImageView animprocress;
    SharedPreferences sharedPreferenceslanguage;
    String language;

    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(NotificationDetails.this);
    UrlServer urlServer=new UrlServer();
    String URL,IMGURL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=(ProgressBar)findViewById(R.id.progressBar2);
        img=(android.widget.ImageView)findViewById(R.id.im_noti);
        //animprocress=(android.widget.ImageView)findViewById(R.id.animprocress);
        scrollView=(ScrollView)findViewById(R.id.scroll);
        tx_title=(TextView)findViewById(R.id.tx_title);
        tx_datetime=(TextView)findViewById(R.id.tx_date);
        tx_msg=(TextView)findViewById(R.id.tx_body);
        progressBar.setVisibility(View.VISIBLE);
        notify_id=getIntent().getExtras().getString("notify_id");


        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        tx_title.setTypeface(face);
        tx_datetime.setTypeface(face);
        tx_msg.setTypeface(face);


        URL=urlServer.getUrl();
        IMGURL=urlServer.getImageURL();
        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
            }
        }
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);
        isInternetOn();

        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(NotificationDetails.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    public final boolean isInternetOn() {

// get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

// Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            new notif().execute();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }
    private class notif extends AsyncTask<String, String, JSONArray> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"admin_notify_details.php?id="+notify_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {
            progressBar.setVisibility(View.GONE);
            try {
                if(json==null){
                    scrollView.setVisibility(View.GONE);
                }else {
                    scrollView.setVisibility(View.VISIBLE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        title = obj.getString("title");
                        content = obj.getString("message");
                        image = obj.getString("image");
                        datetime = obj.getString("date_time");

                    }
                    //      Picasso.with(getApplicationContext()).load("http://mdmarco.in/e-commerce/mart/images/"+image).error(R.drawable.erro).placeholder(R.drawable.progress_animation).into(img);
                    Picasso.with(getApplicationContext()).load(IMGURL+"images/"+image).into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                            //  progressBar.setVisibility(View.GONE);
                            //animprocress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            img.setVisibility(View.GONE);

                            //   animprocress.setVisibility(View.GONE);
                        }
                    });
                    int   count = json.length();

                    tx_title.setText(title);
                    tx_msg.setText(content);
                    tx_datetime.setText(datetime);


                }

            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
}


