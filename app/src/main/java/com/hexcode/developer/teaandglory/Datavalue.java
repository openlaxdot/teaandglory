package com.hexcode.developer.teaandglory;


public class Datavalue {
    String Comment;
String status;
String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    String username;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getProductprice() {
        return Productprice;
    }

    public void setProductprice(String productprice) {
        Productprice = productprice;
    }

    String Productprice;
    String Image;
    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    String productid;
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    String product;
    String price;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    String quantity;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    String house_num,street_name,area,street_num,zone,adr_id;

    public String getHouse_num() {
        return house_num;
    }

    public String getStreet_name() {
        return street_name;
    }

    public String getArea() {
        return area;
    }

    public String getStreet_num() {
        return street_num;
    }

    public String getZone() {
        return zone;
    }

    public void setHouse_num(String house_num) {
        this.house_num = house_num;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setStreet_num(String street_num) {
        this.street_num = street_num;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAdr_id() {
        return adr_id;
    }

    public void setAdr_id(String adr_id) {
        this.adr_id = adr_id;
    }
}
