package com.hexcode.developer.teaandglory;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG="MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

     sendnotification(remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle());


        Log.d(TAG,"FROM  :"+remoteMessage.getFrom());
        if(remoteMessage.getData().size()>0){
            Log.d(TAG,"Message Data : "+remoteMessage.getData());
        }
        if(remoteMessage.getNotification()!=null){
            Log.d(TAG,"Message Data : "+remoteMessage.getNotification().getBody());


        }
    }
    private void sendnotification(String body, String title) {


        Intent resultIntent = new Intent(this,NotificationList.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,resultIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Snack Maroc")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                .setContentText(body)
                .setSmallIcon(R.drawable.icon_snackmaroc) .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.icon_snackmaroc))
                .setAutoCancel(true);

        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification
        mNotifyBuilder.setContentText(body);
        // Set autocancel
        mNotificationManager.notify(0, mNotifyBuilder.build());
    }
}
