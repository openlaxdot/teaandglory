package com.hexcode.developer.teaandglory;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Marco on 10-11-2016.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG="MyFirebaseInsIdService";
    String refreshedtocken;

    @Override
    public void onTokenRefresh() {
         refreshedtocken= FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"new tocken : "+refreshedtocken);
    }

}
