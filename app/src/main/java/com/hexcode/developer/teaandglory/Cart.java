package com.hexcode.developer.teaandglory;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Cart extends AppCompatActivity {
    TextView msg,price_total;
    int postion;

    DatabaseCart databaseCart=new DatabaseCart(Cart.this);
    Datavalue datavalue=new Datavalue();
    String price;
    int count;

    LinearLayout linearLayout;
    ArrayAdapter<String> adapter1;
    String[]name_list=new String[10000];
    String[]id_list=new String[10000];
    int searchcount;
    ListView lv;
    String product_id;
    ProgressBar progressBar;
    double totelamont=0;
    RecyclerView cartRecyclerView;
    String[] slno=new String[100000];
    String[] Itemname=new String[100000];
    String[] itemPrice=new String[100000];
    String[] Quantity=new String[100000];
    String[] totelPrice=new String[100000];
    String[] image=new String[100000];
    String[] status=new String[100000];
    TextView mycart,tx_total_price,tx_total;
    Cursor c;
    static View.OnClickListener myOnClickListener;
    Handler   hcount;
    double total_price=0;
    LinearLayout check_ll;
    Button checkout;

    SharedPreferences sharedPreferenceslanguage;
    String language;
    UrlServer urlServer=new UrlServer();
    String URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=(ProgressBar)findViewById(R.id.progressBar1);
        check_ll=(LinearLayout)findViewById(R.id.check_ll);
        checkout=(Button)findViewById(R.id.checkout);
        tx_total_price=(TextView)findViewById(R.id.tx_total_price);
        cartRecyclerView=(RecyclerView)findViewById(R.id.cartrecycle) ;
        lv=(ListView)findViewById(R.id.list);
        myOnClickListener = new MyOnClickListener(this);
        mycart=(TextView)findViewById(R.id.mycart);
        msg=(TextView)findViewById(R.id.msg);
        tx_total=(TextView)findViewById(R.id.tx_total);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        URL=urlServer.getUrl();
        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        checkout.setText("Checkout Now");
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                mycart.setText("My Cart");
                tx_total.setText("Total Amount : ");
                checkout.setText("Order now");
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
            }
            else if("ARABIC".matches(language))
            {
                mycart.setText("سلتي");
                tx_total.setText("المبلغ الإجمالي");
                checkout.setText("طلب الآن");
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
            }
        }
        isInternetOn();
        lv.setDivider(null);
        lv.setDividerHeight(0);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String product=lv.getItemAtPosition(position).toString();

                int index = -1;
                for (int i=0;i<name_list.length;i++) {
                    if (name_list[i].equals(product)) {
                        index = i;
                        break;
                    }
                }
                product_id=id_list[index];
            }
        });

        c=databaseCart.getProducts();
        c.moveToFirst();
        if(c.getCount()==0)
        {
            check_ll.setVisibility(View.GONE);
            msg.setVisibility(View.VISIBLE);
        }
        else
        {
            Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                    "LibreFranklin-Light.ttf");
            checkout.setTypeface(face);
            msg.setTypeface(face);
            tx_total.setTypeface(face);
            mycart.setTypeface(face);
            tx_total_price.setTypeface(face);

            check_ll.setVisibility(View.VISIBLE);
            msg.setVisibility(View.GONE);
            int s=0;

            for (int i=0;i<c.getCount();i++){
                c.moveToPosition(i);
                s=s+1;

                // totelamont=totelamont+Double.parseDouble(c.getString(3));
                slno[i]= Integer.toString(s);
                Itemname[i]=c.getString(1);
                itemPrice[i]=c.getString(6);
                Quantity[i]=c.getString(2);
                totelPrice[i]=c.getString(3);
                image[i]=c.getString(7);
                double item_price=Double.parseDouble(totelPrice[i]);
                total_price=total_price+item_price;
            }

            double total_round=round(total_price,2);
            tx_total_price.setText(String.format("%.2f", total_round));
            checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(Cart.this,AdrresList.class);
                    i.putExtra("total",tx_total_price.getText().toString());
                    startActivity(i);
                    finish();
                }
            });
            RecyclerView.LayoutManager recylerViewLayoutManager=new LinearLayoutManager(Cart.this);
            CartAdapter cartAdapter=new CartAdapter(Cart.this,slno,Itemname,itemPrice,Quantity,totelPrice,image,c.getCount(),status);
            cartRecyclerView.setLayoutManager(recylerViewLayoutManager);
            cartRecyclerView.setAdapter(cartAdapter);
            cartRecyclerView.invalidate();
            cartAdapter.notifyDataSetChanged();

        }
    }
    private class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            finish();
            startActivity(getIntent());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    final boolean isInternetOn() {

// get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

// Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            new SearchConnect().execute();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }


    private class SearchConnect extends AsyncTask<String, String, JSONArray> {

        String data;
        List<String> product=new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();



        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"allproduct.php");
            return json;
        }

        @Override
        protected void onPostExecute(JSONArray json) {
            try {

                if(json==null){

                }else {
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String id=obj.getString("product_id");
                        String photo = obj.getString("product_name");
                        product.add(photo);
                        name_list[i]=photo;
                        id_list[i]=id;
                    }
                    searchcount = json.length();
                    adapter1=new ArrayAdapter(getApplicationContext(),R.layout.spinnerlayout,product);
                    lv.setAdapter(adapter1);

                    Log.d("card......", json.toString());
                }
                //pDialog.dismiss();


            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public void refresh(){

        finish();
        startActivity(getIntent());
    }

}
