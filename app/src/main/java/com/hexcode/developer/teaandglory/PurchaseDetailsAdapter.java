package com.hexcode.developer.teaandglory;

/**
 * Created by MarcoBravia on 28-01-2017.
 */


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PurchaseDetailsAdapter extends RecyclerView.Adapter<PurchaseDetailsAdapter.ViewHolder> {
    View view1;
    ViewHolder viewHolder1;
    Context pcContext;
    String[] ProductName;
    String[] ProductQuantity;
    String[] ProductPrice;

    int pcount;
    SharedPreferences sharedPreferenceslanguage;
    String language;
    public PurchaseDetailsAdapter(Context context, String[] product_list, String[] quantity_list, String[] price_list, int count1){
        pcContext=context;
        ProductName=product_list;
        ProductQuantity=quantity_list;
        ProductPrice=price_list;
        pcount=count1;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchasedetails, parent, false);


        viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.product.setText(ProductName[position]);
        holder.quantity.setText(ProductQuantity[position]);
        holder.price.setText(ProductPrice[position]);
        sharedPreferenceslanguage=pcContext.getSharedPreferences(MainPage.lang,Context.MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                holder.tx_product.setText("Product");
                holder.tx_quantity.setText("Quantity");
                holder.tx_price.setText("Price");
            }
            else if("ARABIC".matches(language))
            {
                holder.tx_product.setText("المنتج");
                holder.tx_quantity.setText("كمية");
                holder.tx_price.setText("السعر");
            }
        }
    }

    @Override
    public int getItemCount() {
        return pcount;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        public TextView product,price,quantity,tx_product,tx_price,tx_quantity;
        public ViewHolder(View v) {
            super(v);
            product = (TextView)v.findViewById(R.id.product);
            price = (TextView)v.findViewById(R.id.price);
            quantity = (TextView)v.findViewById(R.id.quantity);
            tx_product = (TextView)v.findViewById(R.id.tx_product);
            tx_price = (TextView)v.findViewById(R.id.tx_price);
            tx_quantity = (TextView)v.findViewById(R.id.tx_quantity);

            Typeface face = Typeface.createFromAsset(pcContext.getAssets(),
                    "LibreFranklin-Light.ttf");
            product.setTypeface(face);
            price.setTypeface(face);
            quantity.setTypeface(face);
            tx_product.setTypeface(face);
            tx_price.setTypeface(face);
            tx_quantity.setTypeface(face);
        }
    }
}
