package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    View view1;
    ViewHolder viewHolder1;
Cursor c;
    int pcount;
    Context context1;
    String[] sl,productname,productprice,productquantyty,producttotelPrice,image,Status;
    String product_id,product_name,single_price,product_quantity,product_price,img;

    Datavalue datavalue=new Datavalue();
    DatabaseCart databaseCart;

    SharedPreferences sharedPreferenceslanguage;
    String language;
    UrlServer urlServer=new UrlServer();
    String IMGURL;


        public CartAdapter(Context context, String[] slno, String[] Itemname, String[] itemPrice, String[] Quantity, String[] totelPrice, String[] image, int count, String[] stat){
                this.context1=context;
            this.sl=slno;
            this.productname=Itemname;
            this.productprice=itemPrice;
            this.productquantyty=Quantity;
            this.producttotelPrice=totelPrice;
            this.pcount=count;
            this.image=image;

            databaseCart=new DatabaseCart(context1);
this.Status=stat;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_layout1, parent, false);

        viewHolder1 = new ViewHolder(view1);


        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try{
            IMGURL=urlServer.getImageURL();
            Picasso.with(context1).load(IMGURL+"productimage/"+image[position]).into(holder.cartimageproduct);
            holder.slno.setText(sl[position]);
            holder.product_name.setText(productname[position]);
            holder.product_oneprice.setText(productprice[position]);
            holder.product_quantity.setText(productquantyty[position]);
            holder.product_price.setText(producttotelPrice[position]);
            holder.textView1.setText(productquantyty[position]);

            final int total = Integer.parseInt(productprice[position]) * Integer.parseInt(productquantyty[position]);

            product_price=holder.product_price.getText().toString().trim();
            sharedPreferenceslanguage=context1.getSharedPreferences(MainPage.lang,Context.MODE_PRIVATE);
            language=sharedPreferenceslanguage.getString("lang",null);
            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    holder.tx_item.setText("Item Name");
                    holder.tx_price.setText("Price");
                    holder.tx_quanti.setText("Quantity");
                    holder.tx_total.setText("Total");
                  //  holder.btn_edit.setText("EDIT");
                   // holder.btn_close.setText("DELETE");
                    holder.order.setText("SAVE    Qr"+" "+String.valueOf(total));
                }
                else if("ARABIC".matches(language))
                {
                    holder.tx_item.setText("اسم العنصر");
                    holder.tx_price.setText("السعر");
                    holder.tx_quanti.setText("كمية");
                    holder.tx_total.setText("مجموع");
                    //holder.btn_edit.setText("تصحيح");
                    //holder.btn_close.setText("حذف");
                    holder.order.setText("حفظ ريال قطري"+" "+String.valueOf(total));
                }
            }

            img=image[position];
            holder.btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.edit_delete.setVisibility(View.GONE);
                    holder.edit_ll.setVisibility(View.VISIBLE);
                }
            });
            holder.btn_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i= Integer.parseInt(holder.textView1.getText().toString());
                    i=i+1;
                    holder.textView1.setText(Integer.toString(i));
                    double price= Double.parseDouble(productprice[position]);
                    double qnty=Double.parseDouble(holder.textView1.getText().toString());
                    double total=price*qnty;
                    double total_round=round(total,2);
                    String str_total=String.format("%.2f", total_round);
                    product_price=str_total;

                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {
                            holder.order.setText("SAVE    Qr"+" "+str_total);
                        }
                        else if("ARABIC".matches(language))
                        {

                            holder.order.setText("حفظ ريال قطري"+" "+str_total);
                        }
                    }
                }
            });
            holder.btn_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.textView1.getText().equals("1"))
                    {
                        if(language!=null)
                        {
                            if("ENGLISH".matches(language))
                            {
                                Toast.makeText(context1, "can't do this ", Toast.LENGTH_SHORT).show();
                            }
                            else if("ARABIC".matches(language))
                            {
                                Toast.makeText(context1, "لا يمكنك أن تفعل هذا", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(context1, "can't do this ", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        int i= Integer.parseInt(holder.textView1.getText().toString());
                        i=i-1;
                        holder.textView1.setText(Integer.toString(i));
                        double price= Double.parseDouble(productprice[position]);
                        double qnty=Double.parseDouble(holder.textView1.getText().toString());
                        double total=price*qnty;
                        double total_round=round(total,2);
                        String str_total=String.format("%.2f", total_round);
                        product_price=str_total;
                        if(language!=null)
                        {
                            if("ENGLISH".matches(language))
                            {
                                holder.order.setText("SAVE    Qr"+" "+str_total);
                            }
                            else if("ARABIC".matches(language))
                            {

                                holder.order.setText("حفظ ريال قطري"+" "+str_total);
                            }
                        }
                    }
                }
            });
            holder.order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatabaseCart databaseCart=new DatabaseCart(context1);
                    Datavalue datavalue=new Datavalue();
                    c=databaseCart.getProducts();
                    c.moveToPosition(position);
                    product_id=c.getString(4);
                    product_name=productname[position];
                    single_price=productprice[position];
                    product_quantity=holder.textView1.getText().toString();

                    double price= Double.parseDouble(single_price);
                    double qnty=Double.parseDouble(product_quantity);
                    double total=price*qnty;
                    double total_round=round(total,2);
                    String str_total=String.format("%.2f", total_round);
                    product_price=str_total;
                    datavalue.setProductid(product_id);
                            UpdateCartdata();
                    ((Cart)context1).refresh();
                }
            });
            holder.btn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        DatabaseCart databaseCart=new DatabaseCart(context1);
                        Datavalue datavalue=new Datavalue();
                        c=databaseCart.getProducts();
                        c.moveToPosition(position);
                        datavalue.setProductid(c.getString(4));
                        databaseCart.deleteCart(datavalue);
                        databaseCart.close();

                        if(language!=null)
                        {
                            if("ENGLISH".matches(language))
                            {
                                Toast.makeText(context1, "Deleted", Toast.LENGTH_SHORT).show();
                            }
                            else if("ARABIC".matches(language))
                            {
                                Toast.makeText(context1, "تم الحذف", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(context1, "Deleted", Toast.LENGTH_SHORT).show();
                        }
                        ((Cart)context1).refresh();
                    }catch (Exception e){

                        Log.d("ERROR",e.getMessage());
                        Toast.makeText(context1, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });




        }catch (Exception e){

            Log.e("err",e.toString());
        }

    }

    @Override
    public int getItemCount() {
        return pcount;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        public ImageView cartimageproduct;
        public Button btn_edit,btn_close;
        public LinearLayout edit_ll,edit_delete;

        public TextView slno,product_name,product_oneprice,product_price,product_quantity,textView1;
        public  TextView tx_item,tx_price,tx_quanti,tx_total;

        public Button order,btn_plus,btn_minus;

        public ViewHolder(View v) {

            super(v);
                slno = (TextView)v.findViewById(R.id.slno);
             product_name = (TextView) v.findViewById(R.id.product);
             product_price = (TextView) v.findViewById(R.id.price);
             product_quantity = (TextView) v.findViewById(R.id.product_quantity);
             product_oneprice = (TextView) v.findViewById(R.id.oneprice);

             btn_close = (Button) v.findViewById(R.id.close);
             btn_edit = (Button) v.findViewById(R.id.edit);
             cartimageproduct=(ImageView)v.findViewById(R.id.procartimag) ;

            tx_item= (TextView) v.findViewById(R.id._item);
            tx_price= (TextView) v.findViewById(R.id._price);
            tx_quanti= (TextView) v.findViewById(R.id._quanti);
            tx_total= (TextView) v.findViewById(R.id._total);
            textView1 = (TextView)v.findViewById(R.id.tx_qnty);
            btn_plus=(Button)v.findViewById(R.id.btn_plus);
            btn_minus=(Button)v.findViewById(R.id.btn_minus);
            order=(Button)v.findViewById(R.id.order);
            edit_delete=(LinearLayout)v.findViewById(R.id.edit_delete);
            edit_ll=(LinearLayout)v.findViewById(R.id.edit_ll);

            Typeface face = Typeface.createFromAsset(context1.getAssets(),
                    "LibreFranklin-Light.ttf");
            btn_close.setTypeface(face);
            btn_edit.setTypeface(face);
            tx_price.setTypeface(face);
            tx_item.setTypeface(face);
            tx_quanti.setTypeface(face);
            tx_total.setTypeface(face);
            textView1.setTypeface(face);
            btn_plus.setTypeface(face);
            order.setTypeface(face);
            btn_minus.setTypeface(face);
            product_oneprice.setTypeface(face);
            product_quantity.setTypeface(face);
            product_price.setTypeface(face);
            product_name.setTypeface(face);
            slno.setTypeface(face);

        }
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public void UpdateCartdata(){
        try {
            datavalue.setProductid(product_id);
            datavalue.setPrice(product_price);
            datavalue.setProduct(product_name);
            datavalue.setQuantity(product_quantity);
            datavalue.setImage(img);
            datavalue.setProductprice(single_price);
            databaseCart.updatecart(datavalue);
            databaseCart.close();

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(context1, "Updated To Cart", Toast.LENGTH_SHORT).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(context1, "محدث التسوق", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            Toast.makeText(context1,e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
