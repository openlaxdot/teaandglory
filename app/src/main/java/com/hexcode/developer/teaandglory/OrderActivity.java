package com.hexcode.developer.teaandglory;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderActivity extends AppCompatActivity {
    TextInputLayout layout_name, layout_mbl, layout_housenum, layout_streetname, layout_area, layout_streetnum;
    EditText ed_name, ed_mbl, ed_housenum, ed_streetname, ed_area, ed_streetnum;
    Spinner ed_zone;
    Button order;
    DatabaseCart databaseCart = new DatabaseCart(OrderActivity.this);
    Datavalue datavalue = new Datavalue();
    String device_id, cart_id, date, time, name, phone, housenum, streetname, area, streetnum, zone, total;
    JSONParser jsonParser = new JSONParser();
    int res = 0, res1 = 0;
    SharedPreferences sharedPreferenceslanguage;
    String language;

    TextView street1,zone1,ph1;

    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    LinearLayout ll1;
    RecyclerView recyclerView;
    Cursor c;
    String[] id = new String[10000];
    String[] slno = new String[1000];
    String[] name_list = new String[1000];
    String[] phone_list = new String[1000];
    String[] house_num_list = new String[1000];
    String[] street_name_list = new String[1000];
    String[] area_list = new String[1000];
    String[] street_num_list = new String[1000];
    String[] zone_list = new String[1000];
    Button add_new;
    ScrollView ll2;

    String Id,status;
    UrlServer urlServer=new UrlServer();
    String URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layout_name = (TextInputLayout) findViewById(R.id.layout_name);
        layout_mbl = (TextInputLayout) findViewById(R.id.layout_mbl);
        layout_housenum = (TextInputLayout) findViewById(R.id.layout_housenum);
        layout_streetname = (TextInputLayout) findViewById(R.id.layout_streetname);
        layout_area = (TextInputLayout) findViewById(R.id.layout_area);
        layout_streetnum = (TextInputLayout) findViewById(R.id.layout_streetnum);
        ed_name = (EditText) findViewById(R.id.ed_name);
        ed_mbl = (EditText) findViewById(R.id.ed_mbl);
        ed_housenum = (EditText) findViewById(R.id.ed_housenum);
        ed_streetname = (EditText) findViewById(R.id.ed_streetname);
        ed_area = (EditText) findViewById(R.id.ed_area);
        ed_streetnum = (EditText) findViewById(R.id.ed_streetnum);
        ed_zone = (Spinner) findViewById(R.id.ed_zone);
        order = (Button) findViewById(R.id.order);
        ll1 = (LinearLayout) findViewById(R.id.ll1);
        ll2 = (ScrollView) findViewById(R.id.ll2);
        recyclerView = (RecyclerView) findViewById(R.id.recycleview);
        add_new = (Button) findViewById(R.id.add_new);
        URL=urlServer.getUrl();
        total = getIntent().getExtras().getString("total");
        status=getIntent().getExtras().getString("from");
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        ph1 = (TextView)findViewById(R.id.ph1);
        street1 = (TextView)findViewById(R.id.street1);
        zone1 = (TextView)findViewById(R.id.zone1);

        addSpinnerItems();

        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        order.setTypeface(face);
        ed_name.setTypeface(face);
        ed_mbl.setTypeface(face);
        ed_housenum.setTypeface(face);
        ed_streetname.setTypeface(face);
        ed_area.setTypeface(face);
        ed_streetnum.setTypeface(face);
        add_new.setTypeface(face);
        ph1.setTypeface(face);
        zone1.setTypeface(face);
        street1.setTypeface(face);

           Id=getIntent().getExtras().getString("id");
           datavalue.setId(Id);
           Cursor cursor = databaseCart.getid(datavalue);
           cursor.moveToFirst();
           if (cursor != null) {
               cursor.moveToFirst();
               if (cursor.getCount() == 0) {

               } else {


                   ed_name.setText(cursor.getString(1));
                   ed_mbl.setText(cursor.getString(2));
                   ed_housenum.setText(cursor.getString(3));
                   ed_streetname.setText(cursor.getString(4));
                   ed_area.setText(cursor.getString(5));
                   ed_streetnum.setText(cursor.getString(6));

               }
           }

            sharedPreferenceslanguage = getSharedPreferences(MainPage.lang, MODE_PRIVATE);
            language = sharedPreferenceslanguage.getString("lang", null);
            hcount = new Handler();
            hcount.postDelayed(runnablecount, 200);

            if (language != null) {

                if ("ENGLISH".matches(language)) {

                    layout_name.setHint(getResources().getString(R.string.nm));
                    layout_mbl.setHint(getResources().getString(R.string.mbl));
                    layout_housenum.setHint(getResources().getString(R.string.hnum));
                    layout_streetname.setHint(getResources().getString(R.string.snm));
                    layout_area.setHint(getResources().getString(R.string.area));
                    layout_streetnum.setHint(getResources().getString(R.string.strnum));

              /*      ed_name.setHint("Name");
                    ed_mbl.setHint("e.g 5487-9658");
                    ed_housenum.setHint("House Number");
                    ed_streetname.setHint("e.g Fereej");
                    ed_area.setHint("Area");
                    ed_streetnum.setHint("Street Number");
                    ed_zone.setHint("e.g Hilal(42)");*/
                    order.setText("Confirm Order");
                    street1.setText("Street");
                    zone1.setText("Branch");
                    ActionBar ab = getSupportActionBar();
                    ab.setTitle(R.string.app_name);
                } else if ("ARABIC".matches(language)) {

                    layout_name.setHint(getResources().getString(R.string.nm1));
                    layout_mbl.setHint(getResources().getString(R.string.mbl1));
                    layout_housenum.setHint(getResources().getString(R.string.hnum1));
                    layout_streetname.setHint(getResources().getString(R.string.snm1));
                    layout_area.setHint(getResources().getString(R.string.area1));
                    layout_streetnum.setHint(getResources().getString(R.string.strnum1));


                   /* ed_name.setHint("الاسم");
                    ed_mbl.setHint("مثل5487-9658");
                    ed_housenum.setHint("رقم الدار");
                    ed_streetname.setHint("مثل فيراج");
                    ed_area.setHint("منطقة");
                    ed_streetnum.setHint("رقم الشارع");
                    ed_zone.setHint("مثل");*/
                    order.setText("طلب");
                    street1.setText("العربي");
                    zone1.setText("منطقة");
                    ActionBar ab = getSupportActionBar();
                    ab.setTitle(R.string.app_name_arabic);
                }
            }
            add_new.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ll1.setVisibility(View.GONE);
                    ll2.setVisibility(View.VISIBLE);
                    order.setVisibility(View.VISIBLE);
                }
            });



            order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    name = ed_name.getText().toString();
                    phone = ed_mbl.getText().toString();
                    housenum = ed_housenum.getText().toString();
                    streetname = ed_streetname.getText().toString();
                    area = ed_area.getText().toString();
                    streetnum = ed_streetnum.getText().toString();
                    zone = String.valueOf(ed_zone.getSelectedItem());
                    if (language != null) {
                        if ("ENGLISH".matches(language)) {
                            submitForm();
                        } else if ("ARABIC".matches(language)) {
                            submitForm1();
                        }
                    }
                }
            });
            android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
            fr_wishlist = (FrameLayout) v.findViewById(R.id.fr_wishlist);
            cou = (TextView) v.findViewById(R.id.tebadge);
            cart = (ImageView) v.findViewById(R.id.prof);
            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(OrderActivity.this, Cart.class);
                    startActivity(i);
                }
            });
            getSupportActionBar().setCustomView(v, lp);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
        }

        private void addSpinnerItems()

        {
            List<String> list = new ArrayList<String>();
            list.add("select");
            list.add("Sharja");
            list.add("Riyad");
            list.add("Oman");
            list.add("Al Jaseera");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ed_zone.setAdapter(dataAdapter);

        }

    private boolean validateName() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            //layout_name.setError("Enter Name");
            requestFocus(ed_name);
            return false;
        } else {
           // layout_name.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateName1() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            //layout_name.setError("أدخل اسم");
            requestFocus(ed_name);
            return false;
        } else {
           // layout_name.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMbl() {
        if (ed_mbl.getText().toString().trim().isEmpty()) {
           // layout_mbl.setError("Enter Mobile Number");
            requestFocus(ed_mbl);
            return false;
        } else {
          //  layout_mbl.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validate1Mbl() {
        if (ed_mbl.getText().toString().trim().isEmpty()) {
           // layout_mbl.setError("أدخل رقم الهاتف الجوال");
            requestFocus(ed_mbl);
            return false;
        } else {
           // layout_mbl.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMbl1() {
        if (ed_mbl.getText().toString().trim().length() < 8) {
           // layout_mbl.setError("Enter Valid Mobile Number");
            requestFocus(ed_mbl);
            return false;
        } else {
           // layout_mbl.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validate1Mbl1() {
        if (ed_mbl.getText().toString().trim().length() < 8) {
           // layout_mbl.setError("ادخل رقم هاتف خلوي ساري المفعول");
            requestFocus(ed_mbl);
            return false;
        } else {
            //layout_mbl.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatehousenum() {
        if (ed_housenum.getText().toString().trim().isEmpty()) {
            //layout_housenum.setError("Enter House Number");
            requestFocus(ed_housenum);
            return false;
        } else {
           // layout_housenum.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatehousenum1() {
        if (ed_housenum.getText().toString().trim().isEmpty()) {
            //layout_housenum.setError("أدخل عدد المنازل");
            requestFocus(ed_housenum);
            return false;
        } else {
           // layout_housenum.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateStreetname() {
        if (ed_streetname.getText().toString().trim().isEmpty()) {
           // layout_streetname.setError("Enter Street Name");
            requestFocus(ed_streetname);
            return false;
        } else {
           // layout_streetname.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateStreetname1() {
        if (ed_streetname.getText().toString().trim().isEmpty()) {
          //  layout_streetname.setError("أدخل اسم الشارع");
            requestFocus(ed_streetname);
            return false;
        } else {
           // layout_streetname.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateArea() {
        if (ed_area.getText().toString().trim().isEmpty()) {
           // layout_area.setError("Enter Area");
            requestFocus(ed_area);
            return false;
        } else {
          //  layout_area.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateArea1() {
        if (ed_area.getText().toString().trim().isEmpty()) {
          //  layout_area.setError("دخول المنطقة");
            requestFocus(ed_area);
            return false;
        } else {
           // layout_area.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateStreetnum() {
        if (ed_streetnum.getText().toString().trim().isEmpty()) {
          //  layout_streetnum.setError("Enter Street Number");
            requestFocus(ed_streetnum);
            return false;
        } else {
           // layout_streetnum.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateStreetnum1() {
        if (ed_streetnum.getText().toString().trim().isEmpty()) {
           // layout_streetnum.setError("أدخل عدد من الشارع");
            requestFocus(ed_streetnum);
            return false;
        } else {
           // layout_streetnum.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateZone() {
        if (String.valueOf(ed_zone.getSelectedItem()).trim().isEmpty()) {
          //  layout_zone.setError("Enter Zone");
            requestFocus(ed_zone);
            return false;
        } else {
          //  layout_zone.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateZone1() {
        if (String.valueOf(ed_zone.getSelectedItem()).trim().isEmpty()) {
           // layout_zone.setError("أدخل المنطقة");
            requestFocus(ed_zone);
            return false;
        } else {
          //  layout_zone.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void submitForm() {
        if (!validateName()) {
            return;
        }

        if (!validateMbl()) {
            return;
        }
        if (!validateMbl1()) {
            return;
        }
//        if (!validatehousenum()) {
//            return;
//        }
//        if (!validateStreetname()) {
//            return;
//        }
//        if (!validateArea()) {
//            return;
//        }
//        if (!validateStreetnum()) {
//            return;
//        }
//        if (!validateZone()) {
//            return;
//        }


        DateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
        dateFormatter.setLenient(false);
        DateFormat timeFormatter = new SimpleDateFormat("hhmma");
        timeFormatter.setLenient(false);
        Date today = new Date();
        date = dateFormatter.format(today);
        time = timeFormatter.format(today);
        cart_id = device_id + date + time;
        new CartAddCart().execute();

    }

    private void submitForm1() {
        if (!validateName1()) {
            return;
        }

        if (!validate1Mbl()) {
            return;
        }
        if (!validate1Mbl1()) {
            return;
        }
//        if (!validatehousenum1()) {
//            return;
//        }
//        if (!validateStreetname1()) {
//            return;
//        }
//        if (!validateArea1()) {
//            return;
//        }
//        if (!validateStreetnum1()) {
//            return;
//        }
//        if (!validateZone1()) {
//            return;
//        }


        DateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
        dateFormatter.setLenient(false);
        DateFormat timeFormatter = new SimpleDateFormat("hhmma");
        timeFormatter.setLenient(false);
        Date today = new Date();
        date = dateFormatter.format(today);
        time = timeFormatter.format(today);
        cart_id = device_id + date + time;
        new CartAddCart().execute();

    }

    public class CartAddCart extends AsyncTask<String, Void, String> {
        ProgressDialog dialog = new ProgressDialog(OrderActivity.this);

        @Override
        protected String doInBackground(String... arg0) {
            final Cursor c = databaseCart.getProducts();
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {

                    try {
                        List<NameValuePair> param = new ArrayList<NameValuePair>();
                        param.add(new BasicNameValuePair("device_id", device_id));
                        param.add(new BasicNameValuePair("cart_id", cart_id));
                        param.add(new BasicNameValuePair("product_id", c.getString(4)));
                        param.add(new BasicNameValuePair("product", c.getString(1)));
                        param.add(new BasicNameValuePair("quantity", c.getString(2)));
                        param.add(new BasicNameValuePair("price", c.getString(3)));
                        Log.d("request!", "starting");
                        JSONObject json = jsonParser.makeHttpRequest(URL+"add_cart.php?", "POST", param);
                        Log.d("Adding product attempt", json.toString());
                        res = json.getInt("code");
                    } catch (Exception e) {
                        return new String("Exception: " + e.toString());
                    }


                    c.moveToNext();
                }

            }
            // TODO Auto-generated method stub

            return null;

        }

        protected void onPreExecute() {
            if (language != null) {
                if ("ENGLISH".matches(language)) {
                    dialog.setMessage("Processing...");
                } else if ("ARABIC".matches(language)) {
                    dialog.setMessage("معالجة...");
                }
            }
            dialog.show();
            dialog.setCancelable(false);
            //show dialog

            super.onPreExecute();
        }

        protected void onPostExecute(String result) {
            dialog.dismiss();
            if (res == 1) {
                new AddSales().execute();


            } else {

            }
        }

    }

    public class AddSales extends AsyncTask<String, Void, String> {
        ProgressDialog dialog = new ProgressDialog(OrderActivity.this);


        @Override
        protected String doInBackground(String... arg0) {


            // TODO Auto-generated method stub
            try {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("device_id", device_id));
                param.add(new BasicNameValuePair("cart_id", cart_id));
                param.add(new BasicNameValuePair("name", name));
                param.add(new BasicNameValuePair("phone", phone));
                param.add(new BasicNameValuePair("housenum", housenum));
                param.add(new BasicNameValuePair("streetname", streetname));
                param.add(new BasicNameValuePair("area", area));
                param.add(new BasicNameValuePair("streetnum", streetnum));
                param.add(new BasicNameValuePair("zone", zone));
                param.add(new BasicNameValuePair("total", total));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"add_sales.php", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res1 = json.getInt("code");
            } catch (Exception e) {
                return new String("Exception: " + e.toString());
            }
            return null;

        }

        protected void onPreExecute() {
            dialog.setCancelable(false);
            if (language != null) {
                if ("ENGLISH".matches(language)) {
                    dialog.setMessage("Processing...");
                } else if ("ARABIC".matches(language)) {
                    dialog.setMessage("معالجة...");
                }
            }
            dialog.show();

            //show dialog

            super.onPreExecute();
        }

        protected void onPostExecute(String result) {
            dialog.dismiss();
            if (res1 == 1) {


                datavalue.setId(Id);
                Cursor cursor=databaseCart.getid(datavalue);
                cursor.moveToFirst();
                if(cursor!=null) {
                    cursor.moveToFirst();
                    if (cursor.getCount() == 0) {
                        insertaddress();
                    } else {

                        updateaddress();
                    }

                }

                    final Cursor c = databaseCart.getProducts();
                c.moveToFirst();
                if (c.moveToFirst()) {
                    while (!c.isAfterLast()) {

                        try {
                            datavalue.setProductid(c.getString(4));
                            datavalue.setPrice(c.getString(3));
                            datavalue.setProduct(c.getString(1));
                            datavalue.setQuantity(c.getString(2));
                            databaseCart.insertpurcahse(datavalue);
                            databaseCart.close();

                        } catch (Exception e) {

                        }
                        c.moveToNext();

                    }
                    c.close();
                }


                databaseCart.deleteall();
                Intent i = new Intent(OrderActivity.this, FinishActivity.class);
                startActivity(i);
                finish();
            } else {
                if (language != null) {
                    if ("ENGLISH".matches(language)) {
                        Toast.makeText(OrderActivity.this, "Something went wrong. Try again", Toast.LENGTH_SHORT).show();
                    } else if ("ARABIC".matches(language)) {
                        Toast.makeText(OrderActivity.this, "هناك شئ غير صحيح.حاول ثانية", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }

    }

    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this, 200);
            try {
                Cursor cursor = null;

                cursor = databaseCart.getcount();
                //  cursor.moveToFirst();
                if (cursor != null) {
                    cursor.moveToFirst();

                    if (cursor.getInt(0) == 0) {

                        cou.setVisibility(View.GONE);

                    } else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            } catch (Exception e) {
                Log.d("RRRRR", e.getMessage());
            }

        }
    };


    public void insertaddress() {
        datavalue.setUsername(name);
        datavalue.setPhone(phone);
        datavalue.setHouse_num(housenum);
        datavalue.setStreet_name(streetname);
        datavalue.setArea(area);
        datavalue.setStreet_num(streetnum);
        datavalue.setZone(zone);

        databaseCart.InsertAddress(datavalue);
        databaseCart.close();

    }

    public void updateaddress() {
        datavalue.setUsername(name);
        datavalue.setPhone(phone);
        datavalue.setHouse_num(housenum);
        datavalue.setStreet_name(streetname);
        datavalue.setArea(area);
        datavalue.setStreet_num(streetnum);
        datavalue.setZone(zone);
        datavalue.setId(Id);
        databaseCart.updateaddress(datavalue);
        databaseCart.close();
        Toast.makeText(this, "Updated address", Toast.LENGTH_SHORT).show();
    }



    public void refresh() {

        finish();
        startActivity(getIntent());
    }


    private class MyOnClickListener implements View.OnClickListener {
        private final Context context;
        private MyOnClickListener(Context context) {
            this.context = context;
        }
        @Override
        public void onClick(View v) {
            int selectedItemPosition = recyclerView.getChildPosition(v);
            int postion = selectedItemPosition;

        }
    }
}

