package com.hexcode.developer.teaandglory;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
public class NotificationList extends AppCompatActivity {

    ProgressBar progressBar;
    String id1,notify_id,device_id;
    public static final String Namepr = "user";
    int count,count1;
    LinearLayout clearll;
    int postion;
    //ListView list;
    TextView msg;
    // ArrayAdapter<String> adapter;
    Button clear;
    JSONParser jsonParser = new JSONParser();
    int res = 0;
    String[]id_list=new String[10000];

    String[] date=new String[1000];

    String[] title_list=new String[1000];


    String[] dat=new String[1000];

    RecyclerView recyclerView;
    NotificationlistAdapter notificationlistAdapter;
    Context context;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    static View.OnClickListener myOnClickListener;
    SharedPreferences sharedPreferenceslanguage;
    String language;
    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(NotificationList.this);
    UrlServer urlServer=new UrlServer();
    String URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myOnClickListener = new MyOnClickListener(this);
        clearll=(LinearLayout)findViewById(R.id.linearLayout);
        progressBar=(ProgressBar)findViewById(R.id.progressBar5);
        msg=(TextView)findViewById(R.id.msg);
        clear=(Button)findViewById(R.id.button);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);
        URL=urlServer.getUrl();


        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        msg.setTypeface(face);
        clear.setTypeface(face);

        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle("Notifications");
                clear.setText("Clear All");
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle("الإشعارات");
                clear.setText("امسح الكل");
            }
        }

        isInternetOn();
        context=NotificationList.this;
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(language!=null)
                {
                    if("ENGLISH".matches(language))
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NotificationList.this);
                        alertDialogBuilder.setMessage("Are you sure,You wanted to clear all notifications ?");

                        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                new ClearNotify().execute();
                            }
                        });

                        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                    else if("ARABIC".matches(language))
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NotificationList.this);
                        alertDialogBuilder.setMessage("هل أنت متأكد أنك تريد مسح جميع الإشعارات ؟");

                        alertDialogBuilder.setPositiveButton("نعم فعلا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                new ClearNotify().execute();
                            }
                        });

                        alertDialogBuilder.setNegativeButton("لا",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
                else
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NotificationList.this);
                    alertDialogBuilder.setMessage("Are you sure,You wanted to clear all notifications ?");

                    alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            new ClearNotify().execute();
                        }
                    });

                    alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            }
        });
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(NotificationList.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    private class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            int selectedItemPosition = recyclerView.getChildPosition(v);
            postion = selectedItemPosition;
            Intent i=new Intent(NotificationList.this,NotificationDetails.class);
            i.putExtra("notify_id",id_list[postion]);
            startActivity(i);
            Datavalue datavalue=new Datavalue();
            DatabaseCart databaseCart=new DatabaseCart(NotificationList.this);


            datavalue.setProductid(id_list[postion]);
            Cursor c=databaseCart.getNotificatioid(datavalue);
            c.moveToFirst();

            //  View vb = LayoutInflater.from(NotificationList.this).inflate(R.layout.layout_notifi, null);

            //  LinearLayout lv=(LinearLayout)v.findViewById(R.id.cli);
            TextView tv=(TextView)v.findViewById(R.id.newshow);
//            lv.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
            tv.setVisibility(View.GONE);

            if(c!=null){
                c.moveToFirst();
                if(c.getCount()==0){
                    datavalue.setId(id_list[postion]);
                    databaseCart.insertNotificatio(datavalue);
                    databaseCart.close();

                }else{

                }



            }

        }



    }


    private class Connect1 extends AsyncTask<String, String, JSONArray> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"notify_status.php?user="+device_id);
            return json;
        }

        @Override
        protected void onPostExecute(JSONArray json) {

            try {

                if(json==null){

                }else{

                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        notify_id = obj.getString("latest");
                        id1 = obj.getString("clear_id");
                    }
                    count = json.length();
                    new Connect2().execute();

                }



            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }

    private class Connect2 extends AsyncTask<String, String, JSONArray> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();


            JSONArray json = jParser.getJSONFromUrl(URL+"admin_notifications.php?id="+notify_id+"&id1="+id1);
//            JSONArray json = jParser.getJSONFromUrl("http://mdmarco.in/e-commerce/mall/app/admin_notifications.php?id=2&id1=0");
            return json;
        }

        @Override
        protected void onPostExecute(JSONArray json) {
            progressBar.setVisibility(View.GONE);
            try {

                if(json==null){
                    msg.setVisibility(View.VISIBLE);
                    clear.setVisibility(View.GONE);
                    clearll.setVisibility(View.GONE);
                    if(language!=null)
                    {
                        if("ENGLISH".matches(language))
                        {
                            msg.setText("No Notifications");
                        }
                        else if("ARABIC".matches(language))
                        {
                            msg.setText("لا إشعارات");
                        }
                    }
                }else{
                    clearll.setVisibility(View.VISIBLE);
                    clear.setVisibility(View.VISIBLE);
                    msg.setVisibility(View.GONE);

                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String id=obj.getString("id");
                        String title = obj.getString("title");
                        String content = obj.getString("message");
                        String datetime = obj.getString("date_time");
                        id_list[i]=id;
                        date[i]=datetime;
                        title_list[i]=title;
                        id_list[i]=id;}
                    count1 = json.length();
                }

                context = getApplicationContext();


                recyclerView = (RecyclerView) findViewById(R.id.notificationview);

                recylerViewLayoutManager = new LinearLayoutManager(context);

                recyclerView.setLayoutManager(recylerViewLayoutManager);

                notificationlistAdapter=new NotificationlistAdapter(context,title_list,date,id_list,count1);

                recyclerView.setAdapter(notificationlistAdapter);


            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error...", e.getMessage());
            }

        }
    }



    public class ClearNotify extends AsyncTask<String, Void, String>
    {
        ProgressDialog dialog = new ProgressDialog(NotificationList.this);


        @Override
        protected String doInBackground(String... arg0) {


            // TODO Auto-generated method stub
            try{
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("customer_id",device_id));
                param.add(new BasicNameValuePair("notify_id",notify_id));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"update_clear_id.php?", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res =json.getInt("code");
            }
            catch(Exception e){
                return new String("Exception: " + e.toString());
            }
            return null;

        }
        protected void onPreExecute() {


            //show dialog

            super.onPreExecute();

            dialog.setCancelable(false);
            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    dialog.setMessage("Please wait...");
                }
                else if("ARABIC".matches(language))
                {
                    dialog.setMessage("أرجو الإنتظار...");
                }
            }
            else
            {
                dialog.setMessage("Please wait...");
            }

            dialog.show();
        }
        protected void onPostExecute(String result)
        {
            dialog.dismiss();
            if(res==1)
            {
                finish();
                startActivity(getIntent());
            }
            else
            {
                if(language!=null)
                {
                    if("ENGLISH".matches(language))
                    {
                        Toast.makeText(NotificationList.this, "Something went wrong. Try again", Toast.LENGTH_SHORT).show();
                    }
                    else if("ARABIC".matches(language))
                    {
                        Toast.makeText(NotificationList.this, "هناك شيء خاطئ. حاول ثانية", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(NotificationList.this, "Something went wrong. Try again", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }
    public final boolean isInternetOn() {

// get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

// Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
//new Connect1().execute();
// if connected with internet
            new Connect1().execute();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return false;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            finish();


            event.startTracking();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
}

