package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener{

    private Button imageButton;
    private TextView title1,title2;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final Integer[] XMEN= {R.drawable.one,R.drawable.two,R.drawable.three,R.drawable.four};
    private ArrayList<Integer> XMENArray = new ArrayList<Integer>();




    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private SignInButton btnSignIn;


    @Override
    protected void onPause()
    {
        super.onPause();
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        imageButton = (Button)findViewById(R.id.startNow);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Start now", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainPage.class);
                startActivity(intent);
            }
        });

        init();


        btnSignIn = (SignInButton)findViewById(R.id.btn_sign_in);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        try{

            mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }catch (Exception e)
        {
            e.printStackTrace();
        }



        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);*/

                try
                {
                    signIn();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });



    }


   private void init() {
        for(int i=0;i<XMEN.length;i++)
            XMENArray.add(XMEN[i]);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlideAdapter(MainActivity.this,XMENArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);


    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      /*  if (requestCode == 777) {
            handleSignInResponse(resultCode, data);
        }

        callbackManager.onActivityResult(requestCode,resultCode,data);*/


        if (requestCode == RC_SIGN_IN) {


            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        //  Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }


    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {


            GoogleSignInAccount acct = result.getSignInAccount();
            try{
                if(acct.getPhotoUrl()==null){
                    String personPhotoUrl = "http://enadcity.org/enadcity/wp-content/uploads/2017/02/profile-pictures.png";
                    String email = acct.getEmail();
                    String personName = acct.getDisplayName();

                    //  Toast.makeText(getActivity(),email+personName+personPhotoUrl,Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPreferences = getSharedPreferences("Account", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor3 = sharedPreferences.edit();
                    editor3.putString("name", personName);
                    editor3.putString("mail", email);
                    editor3.putString("photo", personPhotoUrl);
                    editor3.putString("id","1");
                    editor3.commit();
                    editor3.apply();


                    Intent i = new Intent(getApplicationContext(), AccountActivity.class);
                    startActivity(i);




                }else
                {
                    String personPhotoUrl = acct.getPhotoUrl().toString();
                    String email = acct.getEmail();
                    String personName = acct.getDisplayName();

                    //   Toast.makeText(getActivity(),email+personName+personPhotoUrl,Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPreferences = getSharedPreferences("Account", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor3 = sharedPreferences.edit();
                    editor3.putString("name", personName);
                    editor3.putString("mail", email);
                    editor3.putString("photo", personPhotoUrl);
                    editor3.putString("id","1");
                    editor3.commit();
                    editor3.apply();

                    Intent i = new Intent(getApplicationContext(), AccountActivity.class);
                    startActivity(i);

                }


            }catch (Exception e)
            {
                e.printStackTrace();
            }



        }
    }



}