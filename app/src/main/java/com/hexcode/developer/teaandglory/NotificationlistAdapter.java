package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by JUNED on 6/10/2016.
 */
public class NotificationlistAdapter extends RecyclerView.Adapter<NotificationlistAdapter.ViewHolder>{



   public Context context;
    View view1;
    ViewHolder viewHolder1;

   int count;

    String[] date;
    String[] title;
    String[] id;
    public NotificationlistAdapter(Context context1, String[] Title, String[] Date, String[] id, int counte){
        count=counte;

        context = context1;

        this.date=Date;
        this.title=Title;
        this.id=id;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView DATE,TITLR,newtex;
    /// public    LinearLayout lv;
        public ViewHolder(final View v){

            super(v);
     //   lv=(LinearLayout)v.findViewById(R.id.cli);

            DATE = (TextView)v.findViewById(R.id.tx_date);
            TITLR=(TextView)v.findViewById(R.id.tx_title);
            newtex=(TextView)v.findViewById(R.id.newshow);


        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notifi, parent, false);


        viewHolder1 = new ViewHolder(view1);
        view1.setOnClickListener(NotificationList.myOnClickListener);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){

        holder.DATE.setText(date[position]);
        holder.TITLR.setText(title[position]);

        Datavalue datavalue=new Datavalue();
        DatabaseCart databaseCart=new DatabaseCart(context);


        datavalue.setProductid(id[position]);
        Cursor c=databaseCart.getNotificatioid(datavalue);
        c.moveToFirst();

        //  View vb = LayoutInflater.from(NotificationList.this).inflate(R.layout.layout_notifi, null);

        if(c!=null){
            c.moveToFirst();
            if(c.getCount()==0){

           //     holder.lv.setBackgroundColor(Color.parseColor("#eeeded"));
                holder.newtex.setVisibility(View.VISIBLE);

            }else{
              //  holder.lv.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                holder.newtex.setVisibility(View.GONE);
            }



        }



    }

    @Override
    public int getItemCount(){

        return count;
    }

}

