package com.hexcode.developer.teaandglory;

/**
 * Created by MarcoBravia on 28-01-2017.
 */


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    View view1;
    ViewHolder viewHolder1;
    Context pcContext;
    String[] Name;
    String[] Phone;
    String[] HouseNumber;
    String[] StreetName;
    String[] Area;
    String[] StreetNumber;
    String[] Zone;
    String[] SlNo;
    String[] id;
    int pcount;
    String total,device_id,date,time,cart_id,name,phone,housenum,streetname,area,streetnum,zone;
    Datavalue datavalue=new Datavalue();
    DatabaseCart databaseCart;
    JSONParser jsonParser = new JSONParser();
    int res = 0, res1 = 0;
    SharedPreferences sharedPreferences;
    String language;
    UrlServer urlServer=new UrlServer();
    String URL;
    public AddressAdapter(Context context,String[] id, String[] slno,String[] name_list, String[] phone_list,String[] house_num_list,String[] street_name_list,String[] area_list,String[] street_num_list,String[] zone_list, int count,String total){
        pcContext=context;
        Name=name_list;
        Phone=phone_list;
        HouseNumber=house_num_list;
        StreetName=street_name_list;
        Area=area_list;
        StreetNumber=street_num_list;
        Zone=zone_list;
        SlNo=slno;
        pcount=count;
        this.id=id;
        this.total=total;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.address, parent, false);


        viewHolder1 = new ViewHolder(view1);
        databaseCart=new DatabaseCart(pcContext);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        URL=urlServer.getUrl();
        sharedPreferences=pcContext.getSharedPreferences(MainPage.lang,Context.MODE_PRIVATE);
        language=sharedPreferences.getString("lang",null);


        if("ENGLISH".matches(language))
        {
            holder.tx_edit.setText("EDIT");
            holder.tx_delete.setText("DELETE");
            holder.tx_slno.setText("SL No ");
            holder.tx_name.setText("Name");
            holder.tx_phone.setText("Phone Number");
            holder.tx_house_num.setText("House Number");
            holder.tx_street_name.setText("Street Name");
            holder.tx_area.setText("Area");
            holder.tx_street_num.setText("Street Number");
            holder.tx_zone.setText("Branch");
            holder.confirm_address.setText("Confirm Address");

        }
        else if("ARABIC".matches(language))
        {
            holder.tx_edit.setText("تصحيح");
            holder.tx_delete.setText("حذف");
            holder.tx_slno.setText("الرقم التسلسلي");
            holder.tx_name.setText("اسم");
            holder.tx_phone.setText("رقم الهاتف");
            holder.tx_house_num.setText("رقم الدار");
            holder.tx_street_name.setText("اسم الشارع");
            holder.tx_area.setText("منطقة");
            holder.tx_street_num.setText("رقم الشارع");
            holder.tx_zone.setText("منطقة");
            holder.confirm_address.setText("تأكيد عنوان");
        }
        device_id = Settings.Secure.getString(pcContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        holder.slno.setText(SlNo[position]);
        holder.name.setText(Name[position]);
        holder.phone.setText(Phone[position]);
        holder.house_num.setText(HouseNumber[position]);
        holder.street_name.setText(StreetName[position]);
        holder.area.setText(Area[position]);
        holder.street_num.setText(StreetNumber[position]);
        holder.zone.setText(Zone[position]);
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(pcContext,OrderActivity.class);
                i.putExtra("total",total);
                i.putExtra("id",id[position]);
                pcContext.startActivity(i);
                ((AdrresList)pcContext).exit();

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                datavalue.setId(id[position]);
                databaseCart.deleteaddress(datavalue);
                databaseCart.close();
                ((AdrresList)pcContext).refresh();
            }
        });
        holder.confirm_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
                dateFormatter.setLenient(false);
                DateFormat timeFormatter = new SimpleDateFormat("hhmma");
                timeFormatter.setLenient(false);
                Date today = new Date();
                date = dateFormatter.format(today);
                time = timeFormatter.format(today);
                cart_id = device_id + date + time;
                name=Name[position];
                phone=Phone[position];
                housenum=HouseNumber[position];
                streetname=StreetName[position];
                area=Area[position];
                streetnum=StreetNumber[position];
                zone=Zone[position];
                new CartAddCart().execute();
            }
        });

    }

    @Override
    public int getItemCount() {
        return pcount;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        public TextView tx_edit,tx_delete,tx_slno,tx_name,tx_phone,tx_house_num,tx_street_name,tx_area,tx_street_num,tx_zone,slno,name,phone,house_num,street_name,area,street_num,zone;
        public CardView edit,delete;
        public Button confirm_address;
        public ViewHolder(View v) {
            super(v);
            tx_slno=(TextView)v.findViewById(R.id.tx_slno);
            tx_name = (TextView)v.findViewById(R.id.tx_name);
            tx_phone = (TextView)v.findViewById(R.id.tx_phone);
            tx_house_num = (TextView)v.findViewById(R.id.tx_house_num);
            tx_street_name = (TextView)v.findViewById(R.id.tx_street_name);
            tx_street_num = (TextView)v.findViewById(R.id.tx_street_num);
            tx_zone = (TextView)v.findViewById(R.id.tx_zone);
            tx_area = (TextView)v.findViewById(R.id.tx_area);
            slno=(TextView)v.findViewById(R.id.slno);
            name = (TextView)v.findViewById(R.id.name);
            phone = (TextView)v.findViewById(R.id.phone);
            house_num = (TextView)v.findViewById(R.id.house_num);
            street_name = (TextView)v.findViewById(R.id.street_name);
            area = (TextView)v.findViewById(R.id.area);
            street_num = (TextView)v.findViewById(R.id.street_num);
            zone = (TextView)v.findViewById(R.id.zone);
            edit=(CardView)v.findViewById(R.id.edit);
            delete=(CardView)v.findViewById(R.id.delete);
            confirm_address=(Button)v.findViewById(R.id.confirm_address);
            tx_edit=(TextView)v.findViewById(R.id.tx_edit);
            tx_delete=(TextView)v.findViewById(R.id.tx_delete);

            Typeface face = Typeface.createFromAsset(pcContext.getAssets(),
                    "LibreFranklin-Light.ttf");
            tx_slno.setTypeface(face);
            tx_name.setTypeface(face);
            tx_phone.setTypeface(face);
            tx_name.setTypeface(face);
            tx_house_num.setTypeface(face);
            tx_street_name.setTypeface(face);
            tx_street_num.setTypeface(face);
            tx_zone.setTypeface(face);
            tx_area.setTypeface(face);
            slno.setTypeface(face);

            tx_delete.setTypeface(face);
            tx_edit.setTypeface(face);
            confirm_address.setTypeface(face);
            zone.setTypeface(face);
            street_num.setTypeface(face);
            area.setTypeface(face);
            street_name.setTypeface(face);
            house_num.setTypeface(face);
            phone.setTypeface(face);
            name.setTypeface(face);

        }
    }

    public class CartAddCart extends AsyncTask<String, Void, String> {
        ProgressDialog dialog = new ProgressDialog(pcContext);

        @Override
        protected String doInBackground(String... arg0) {
            final Cursor c = databaseCart.getProducts();
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {

                    try {
                        List<NameValuePair> param = new ArrayList<NameValuePair>();
                        param.add(new BasicNameValuePair("device_id", device_id));
                        param.add(new BasicNameValuePair("cart_id", cart_id));
                        param.add(new BasicNameValuePair("product_id", c.getString(4)));
                        param.add(new BasicNameValuePair("product", c.getString(1)));
                        param.add(new BasicNameValuePair("quantity", c.getString(2)));
                        param.add(new BasicNameValuePair("price", c.getString(3)));
                        Log.d("request!", "starting");
                        JSONObject json = jsonParser.makeHttpRequest(URL+"add_cart.php?", "POST", param);
                        Log.d("Adding product attempt", json.toString());
                        res = json.getInt("code");
                    } catch (Exception e) {
                        return new String("Exception: " + e.toString());
                    }


                    c.moveToNext();
                }

            }
            // TODO Auto-generated method stub

            return null;

        }

        protected void onPreExecute() {
            if (language != null) {
                if ("ENGLISH".matches(language)) {
                    dialog.setMessage("Processing...");
                } else if ("ARABIC".matches(language)) {
                    dialog.setMessage("معالجة...");
                }
            }
            dialog.show();
            dialog.setCancelable(false);
            //show dialog

            super.onPreExecute();
        }

        protected void onPostExecute(String result) {
            dialog.dismiss();
            if (res == 1) {
                new AddSales().execute();
            } else {

            }
        }

    }

    public class AddSales extends AsyncTask<String, Void, String> {
        ProgressDialog dialog = new ProgressDialog(pcContext);


        @Override
        protected String doInBackground(String... arg0) {


            // TODO Auto-generated method stub
            try {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("device_id", device_id));
                param.add(new BasicNameValuePair("cart_id", cart_id));
                param.add(new BasicNameValuePair("name", name));
                param.add(new BasicNameValuePair("phone", phone));
                param.add(new BasicNameValuePair("housenum", housenum));
                param.add(new BasicNameValuePair("streetname", streetname));
                param.add(new BasicNameValuePair("area", area));
                param.add(new BasicNameValuePair("streetnum", streetnum));
                param.add(new BasicNameValuePair("zone", zone));
                param.add(new BasicNameValuePair("total", total));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"add_sales.php", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res1 = json.getInt("code");
            } catch (Exception e) {
                return new String("Exception: " + e.toString());
            }
            return null;

        }

        protected void onPreExecute() {
            dialog.setCancelable(false);
            if (language != null) {
                if ("ENGLISH".matches(language)) {
                    dialog.setMessage("Processing...");
                } else if ("ARABIC".matches(language)) {
                    dialog.setMessage("معالجة...");
                }
            }
            dialog.show();

            //show dialog

            super.onPreExecute();
        }

        protected void onPostExecute(String result) {
            dialog.dismiss();
            if (res1 == 1) {

                final Cursor c = databaseCart.getProducts();
                c.moveToFirst();
                if (c.moveToFirst()) {
                    while (!c.isAfterLast()) {

                        try {
                            datavalue.setProductid(c.getString(4));
                            datavalue.setPrice(c.getString(3));
                            datavalue.setProduct(c.getString(1));
                            datavalue.setQuantity(c.getString(2));
                            databaseCart.insertpurcahse(datavalue);
                            databaseCart.close();

                        } catch (Exception e) {

                        }
                        c.moveToNext();

                    }
                    c.close();
                }


                databaseCart.deleteall();
                Intent i = new Intent(pcContext, FinishActivity.class);
                pcContext.startActivity(i);
                ((AdrresList)pcContext).exit();
            } else {
                if (language != null) {
                    if ("ENGLISH".matches(language)) {
                        Toast.makeText(pcContext, "Something went wrong. Try again", Toast.LENGTH_SHORT).show();
                    } else if ("ARABIC".matches(language)) {
                        Toast.makeText(pcContext, "هناك شئ غير صحيح.حاول ثانية", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }

    }
}
