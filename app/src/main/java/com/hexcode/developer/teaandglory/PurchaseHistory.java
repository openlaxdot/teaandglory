package com.hexcode.developer.teaandglory;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PurchaseHistory extends AppCompatActivity {

    ProgressBar progressBar;
    TextView msg,category_name;
    LinearLayout p_ll,main_ll;
    ListView p_list;
    RecyclerView recyclerView;
    int count;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    PurchaseAdapter purchasceAdapter;
    static View.OnClickListener myOnClickListener;
    String[] purchase_id_list=new String[5000];
    String[] cart_id_list=new String[5000];
    String[] name_list = new String[5000];
    String[] phone_list = new String[5000];
    String[] house_num_list = new String[5000];
    String[] street_name_list = new String[5000];
    String[] area_list = new String[5000];
    String[] street_num_list = new String[5000];
    String[] zone_list = new String[5000];
    String[] total_list = new String[5000];
    String[] date_list = new String[5000];
    String[] time_list = new String[5000];
    String[] status_list=new String[5000];
    String device_id;
    Button clear;

    JSONParser jsonParser = new JSONParser();
    int res=0;
    SharedPreferences sharedPreferenceslanguage;
    String language;

    FrameLayout fr_wishlist;
    ImageView cart;
    TextView cou;
    Handler hcount;
    DatabaseCart databaseCart=new DatabaseCart(PurchaseHistory.this);
    UrlServer urlServer=new UrlServer();
    String URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=(ProgressBar)findViewById(R.id.progressBar1);
        msg=(TextView)findViewById(R.id.msg);
        p_ll=(LinearLayout)findViewById(R.id.BACK3);
        main_ll=(LinearLayout)findViewById(R.id.main_ll);
        p_list=(ListView)findViewById(R.id.list);
        recyclerView=(RecyclerView)findViewById(R.id.recycleview);
        clear=(Button)findViewById(R.id.clr_btn);
        URL=urlServer.getUrl();
        sharedPreferenceslanguage=getSharedPreferences(MainPage.lang,MODE_PRIVATE);
        language=sharedPreferenceslanguage.getString("lang",null);
        if(language!=null)
        {
            if("ENGLISH".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name);
                clear.setText("Clear ALL");
            }
            else if("ARABIC".matches(language))
            {
                ActionBar ab=getSupportActionBar();
                ab.setTitle(R.string.app_name_arabic);
                clear.setText("امسح الكل");
            }
        }
        hcount=new Handler();
        hcount.postDelayed(runnablecount,200);

        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        isInternetOn();
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ClearAll().execute();
            }
        });
        android.support.v7.app.ActionBar.LayoutParams lp = new android.support.v7.app.ActionBar.LayoutParams(android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        View v = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
        fr_wishlist=(FrameLayout)v.findViewById(R.id.fr_wishlist);
        cou=(TextView)v.findViewById(R.id.tebadge);
        cart=(ImageView)v.findViewById(R.id.prof) ;
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(PurchaseHistory.this,Cart.class);
                startActivity(i);
            }
        });
        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }
    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            new ProductConnect().execute();
            return true;
        } else if (

                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED ) {

            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection ", Toast.LENGTH_LONG).show();
                }
                else if("ARABIC".matches(language))
                {
                    Toast.makeText(getApplicationContext(), "الرجاء التحقق من اتصال الانترنت الخاص بك", Toast.LENGTH_LONG).show();
                }
            }
            return false;
        }
        return false;
    }
    private class ProductConnect extends AsyncTask<String, String, JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            //   Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(URL+"sales_details.php?device_id="+device_id);
            return json;
        }
        @Override
        protected void onPostExecute(JSONArray json) {

            progressBar.setVisibility(View.GONE);
            try {
                if(json==null){
                    msg.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    clear.setVisibility(View.GONE);
                }else {
//                    new Products.SubCategory().execute();
                    clear.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    msg.setVisibility(View.GONE);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        String id=obj.getString("id");
                        String cart_id=obj.getString("cart_id");
                        String customer_name = obj.getString("customer_name");
                        String customer_phone = obj.getString("customer_phone");
                        String house_num = obj.getString("housenumber");
                        String street_name= obj.getString("street_name");
                        String area = obj.getString("area");
                        String street_number = obj.getString("street_number");
                        String zone = obj.getString("zone");
                        String total = obj.getString("total");
                        String date = obj.getString("date");
                        String time = obj.getString("time");
                        String status = obj.getString("status");
                        purchase_id_list[i]=id;
                        cart_id_list[i]=cart_id;
                        name_list[i]=customer_name;
                        phone_list[i]=customer_phone;
                        house_num_list[i]=house_num;
                        street_name_list[i]=street_name;
                        area_list[i]=area;
                        street_num_list[i]=street_number;
                        zone_list[i]=zone;
                        total_list[i]=total;
                        date_list[i]=date;
                        time_list[i]=time;
                        status_list[i]=status;
                    }
                    count = json.length();

                    recylerViewLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(recylerViewLayoutManager);

                    purchasceAdapter = new PurchaseAdapter(getApplicationContext(),purchase_id_list,cart_id_list,name_list,phone_list,house_num_list,street_name_list,area_list,street_num_list,zone_list,total_list,date_list,time_list,status_list,count);
                    recyclerView.setAdapter(purchasceAdapter);
                    Log.d("card......", json.toString());
                }
            } catch (JSONException e) {e.printStackTrace();
                Log.d("error...", e.getMessage());}
        }
    }
    public class ClearAll extends AsyncTask<String, Void, String>
    {
        ProgressDialog dialog = new ProgressDialog(PurchaseHistory.this);


        @Override
        protected String doInBackground(String... arg0) {


            // TODO Auto-generated method stub
            try{
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("device_id",device_id));
                Log.d("request!", "starting");
                JSONObject json = jsonParser.makeHttpRequest(URL+"clear_purchase.php?", "POST", param);
                Log.d("Adding product attempt", json.toString());
                res =json.getInt("code");
            }
            catch(Exception e){
                return new String("Exception: " + e.toString());
            }
            return null;

        }
        protected void onPreExecute() {
            dialog.setCancelable(false);
            if(language!=null)
            {
                if("ENGLISH".matches(language))
                {
                    dialog.setMessage("Clearing...");
                }
                else if("ARABIC".matches(language))
                {
                    dialog.setMessage("المقاصة...");
                }
            }
            dialog.show();

            //show dialog

            super.onPreExecute();
        }
        protected void onPostExecute(String result)
        {
            dialog.dismiss();
            if(res==1)
            {

                finish();
                startActivity(getIntent());
            }
            else
            {
                if(language!=null)
                {
                    if("ENGLISH".matches(language))
                    {

                        Toast.makeText(PurchaseHistory.this, "Something went wrong. Try again", Toast.LENGTH_SHORT).show();
                    }
                    else if("ARABIC".matches(language))
                    {

                        Toast.makeText(PurchaseHistory.this, "هناك شيء خاطئ. حاول ثانية", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    }
    private Runnable runnablecount = new Runnable() {
        @Override
        public void run() {
            hcount.postDelayed(this,200);
            try{
                Cursor cursor=null;

                cursor=databaseCart.getcount();
                //  cursor.moveToFirst();
                if(cursor!=null){
                    cursor.moveToFirst();

                    if(cursor.getInt(0)==0) {

                        cou.setVisibility(View.GONE);

                    }else {
                        cou.setVisibility(View.VISIBLE);
                        cou.setText(cursor.getString(0));

                    }
                    cursor.close();
                }
            }catch (Exception e){
                Log.d("RRRRR",e.getMessage());}

        }
    };
}
