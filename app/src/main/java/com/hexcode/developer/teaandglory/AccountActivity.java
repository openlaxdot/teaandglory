package com.hexcode.developer.teaandglory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by hexcode on 2/2/18.
 */

public class AccountActivity extends AppCompatActivity
{
    private TextView profile_name,profile_email;
    private ImageView profile_pic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        profile_email = (TextView)findViewById(R.id.profile_email);
        profile_name = (TextView)findViewById(R.id.profile_name);
        profile_pic  = (ImageView)findViewById(R.id.profile_pic);

        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "LibreFranklin-Light.ttf");
        profile_name.setTypeface(face);
        profile_email.setTypeface(face);


        SharedPreferences sharedPreferences = getSharedPreferences("Account", Context.MODE_PRIVATE);
        String nm = sharedPreferences.getString("name","");
        String ml = sharedPreferences.getString("mail","");
        String pu = sharedPreferences.getString("photo","");


        profile_name.setText(nm);
        profile_email.setText(ml);
        Glide.with(getApplicationContext()).load(pu)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(profile_pic);

    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(getApplicationContext(), MainPage.class);
        startActivity(intent);

    }
}
